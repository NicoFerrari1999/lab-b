
creazione tabelle: (dbURL ha un formato simile a "jdbc:postgresql://localhost:5432/lab-b")
    ant -DURL="dbURL" -Dusername="YourUsername" -Dpassword="YourPassword" db_setup

compila progetto:
    ant buildProject

avvia server:
    ant -DURL="dbURL" -Dusername="YourUsername" -Dpassword="YourPassword" run_server

avvia client:
    ant run_client

crea javadoc:
    ant doc

cancella bin:
    ant clean_build
cancella doc:
    ant clean_doc
cancella lib
    ant clean_lib
cancella tutto:
    ant clean_all
