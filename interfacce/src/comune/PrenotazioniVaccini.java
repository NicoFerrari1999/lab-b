package comune;

import java.io.Serializable;
import java.sql.Date;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

public class PrenotazioniVaccini implements Serializable{
	private String nomeCentro;
	private String cf;
	private Date data;
	
	public PrenotazioniVaccini(String nomeCentro, String cf, Date data) {
		this.nomeCentro = nomeCentro;
		this.cf = cf;
		this.data = data;
	}
	
	public String getNomeCentro() {
		return nomeCentro;
	}
	
	public String getCf() {
		return cf;
	}
	
	public Date getData() {
		return data;
	}
}
