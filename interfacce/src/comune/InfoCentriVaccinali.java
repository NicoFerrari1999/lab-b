package comune;

import java.io.Serializable;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

/**
 * Classe oggetto per i dati dei centri vaccinali.
 * 
 * @author Luca
 */
public class InfoCentriVaccinali implements Serializable {
	
	private String nomeCentro;
	private String indirizzo;
	private String tipoCentro;
	
	public InfoCentriVaccinali(String nomeCentro, String indirizzo, String tipoCentro) {
		this.nomeCentro = nomeCentro;
		this.indirizzo = indirizzo;
		this.tipoCentro = tipoCentro;
	}
	
	/**
	 * Restituisce il nome del centro vaccinale.
	 * @return Il nome del centro vaccinale.
	 */
	public String getNomeCentro() {
		return nomeCentro;
	}
	
	/**
	 * Restituisce l'indirizzo del centro vaccinale.
	 * @return L'indirizzo del centro vaccinale.
	 */
	public String getIndirizzo() {
		return indirizzo;
	}
	
	/**
	 * Restituisce il tipo di centro del centro vaccinale.
	 * @return Il tipo di centro del centro vaccinale.
	 */
	public String getTipoCentro() {
		return tipoCentro;
	}
}
