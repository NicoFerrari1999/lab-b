package comune;

import java.io.Serializable;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

/**
 * Classe oggetto per i dati dei cittadini.
 * 
 * @author Luca
 */
public class InfoCittadini implements Serializable {
	private String nome;
	private String cognome;
	
	public InfoCittadini(String nome, String cognome) {
		this.nome = nome;
		this.cognome = cognome;
	}
	
	/**
	 * Restituisce il nome del cittadino.
	 * @return Il nome del cittadino.
	 */
	public String getNomeCittadino() {
		return nome;
	}
	
	/**
	 * Restituisce il cognome del cittadino.
	 * @return Il nome del cittadino.
	 */
	public String getCognomeCittadino() {
		return cognome;
	}
}
