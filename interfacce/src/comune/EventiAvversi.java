package comune;

import java.io.Serializable;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

/**
 * Classe oggetto per i dati degli eventi avversi.
 * 
 * @author Luca
 */
public class EventiAvversi implements Serializable{
	
	private int idVaccinazione;
	private String nomeCentro;
	private String[] evento;
	private Integer[] severita;
	private String[] notes;
	
	
	public EventiAvversi(int idVaccinazione, String nomeCentro, String[] evento, Integer[] severita, String[] notes) {
		this.idVaccinazione = idVaccinazione;
		this.nomeCentro = nomeCentro;
		this.evento = evento;
		this.severita = severita;
		this.notes = notes;
	}
	
	/**
	 * Restituisce l'Id vaccinazione del cittadino registrato.
	 * @return L'Id vaccinazione del cittadino registrato.
	 */
	public int getIdVaccinazione() {
		return idVaccinazione;
	}
	
	/**
	 * Restituisce il nome del centro vaccinale.
	 * @return Il nome del centro vaccinale.
	 */
	public String getNomeCentro() {
		return nomeCentro;
	}
	
	/**
	 * Restituisce la lista degli eventi avversi.
	 * @return La lista degli eventi avversi.
	 */
	public String[] getEvento() {
		return evento;
	}
	
	/**
	 * Restituisce la lista della severit� degli eventi avversi.
	 * @return La lista della severit� degli eventi avversi.
	 */
	public Integer[] getSeverita() {
		return severita;
	}
	
	/**
	 * Restituisce la lista delle note per gli eventi avversi.
	 * @return La lista delle note per gli eventi avversi.
	 */
	public String[] getNotes() {
		return notes;
	}
}
