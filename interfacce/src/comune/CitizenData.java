package comune;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

/**
 * Classe oggetto per i dati dei cittadini registrati.
 * 
 * @author Luca
 */
public class CitizenData implements Serializable {
	private String 	name;
	private String 	surname;
	private String 	cf;
	private String 	email;
	private String 	username;
	private String 	pw;
	
	public CitizenData(String name, String surname, String cf, String email, String username, String pw) {
		this.name 		= name;
		this.surname 	= surname;
		this.cf			= cf;
		this.email		= email;
		this.username	= username;
		this.pw			= pw;
	}
	
	/**
	 * Restituisce il nome del cittadino registrato
	 * @return Il nome del cittadino registrato.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Restituisce il cognome del cittadino registrato.
	 * @return Il cognome del cittadino registrato.
	 */
	public String getSurname() {
		return surname;
	}
	
	/**
	 * Restituisce il CF del cittadino registrato.
	 * @return Il Cf del cittadino registrato.
	 */
	public String getCf() {
		return cf;
	}
	
	/**
	 * Restituisce la email del cittadino registrato.
	 * @return La email del cittadino registrato.
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * Restituisce l'Id vaccinazione del cittadino registrato.
	 * @return L'id vaccinazione del cittadino registrato.
	 */
	public String getUsername() {
		return username;
	}
	
	/**
	 * Restituisce la password del cittadino registrato.
	 * @return La password del cittadino registrato.
	 */
	public String getPw() {
		return pw;
	}
}
