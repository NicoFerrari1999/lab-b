package comune;

import java.io.Serializable;
import java.sql.Date;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

/**
 * Classe oggetto per i dati delle vaccinazioni.
 * 
 * @author Luca
 */
public class RegistrazioniVaccinati implements Serializable {
	private String nomeCentro;
	private String cf;
	private Date dataVaccino;
	private String tipoVaccino;
	private int idVaccinazione;
	
	public RegistrazioniVaccinati(String nomeCentro, String cf, Date dataVaccino, String tipoVaccino, int idVaccinazione) {
		this.nomeCentro = nomeCentro;
		this.cf = cf;
		this.dataVaccino = dataVaccino;
		this.tipoVaccino = tipoVaccino;
		this.idVaccinazione = idVaccinazione;
	}
	
	/**
	 * Restituisce il nome del centro vaccinale presso cui ci si � vaccinati.
	 * @return Il nome del centro vaccinale presso cui ci si � vaccinati.
	 */
	public String getNomeCentro() {
		return nomeCentro;
	}
	
	/**
	 * Restituisce il Cf del cittadino vaccinato in quel centro.
	 * @return Il Cf del cittadino vaccinato in quel centro.
	 */
	public String getCf() {
		return cf;
	}
	
	/**
	 * Restituisce la data della vaccinazione.
	 * @return La data della vaccinazione.
	 */
	public Date getDataVaccino() {
		return dataVaccino;
	}
	
	/**
	 * Restituisce il tipo di vaccino somministrato.
	 * @return Il tipo di vaccino somministrato.
	 */
	public String getTipoVaccino() {
		return tipoVaccino;
	}
	
	/**
	 * Restituisce l'Id della vaccinazione.
	 * @return L'Id della vaccinazione.
	 */
	public int getIdVaccinazione() {
		return idVaccinazione;
	}
	
}
