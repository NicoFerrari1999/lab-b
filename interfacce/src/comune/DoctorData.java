package comune;

import java.io.Serializable;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

/**
 * Classe oggetto per i dati degli operatori sanitari registrati.
 * 
 * @author Luca
 */
public class DoctorData implements Serializable {
	private String name;
	private String surname;
	private String doctorId;
	private String email;
	private String password;

	public DoctorData(String name, String surname, String doctorId, String email, String password) {
		this.name = name;
		this.surname = surname;
		this.doctorId = doctorId;
		this.email = email;
		this.password = password;
	}
	
	/**
	 * Restituisce il nome degli operatori sanitari.
	 * @return Il nome degli operatori sanitari.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Restituise il cognome degli operatori sanitari.
	 * @return Il cognome degli operatori sanitari.
	 */
	public String getSurname() {
		return surname;
	}
	
	/**
	 * Restituisce l'Id degli operatori sanitari.
	 * @return L'Id degli operatori sanitari.
	 */
	public String getDoctorId() {
		return doctorId;
	}
	
	/**
	 * Restituisce l'email degli operatori sanitari.
	 * @return L'email degli operatori sanitari.
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * Restituisce la password degli operatori sanitari.
	 * @return La password degli operatori sanitari.
	 */
	public String getPassword() {
		return password;
	}
}
