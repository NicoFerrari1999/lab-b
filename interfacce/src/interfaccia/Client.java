package interfaccia;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Client extends Remote {

	public abstract void update(int[] statistiche) throws RemoteException;
	
}
