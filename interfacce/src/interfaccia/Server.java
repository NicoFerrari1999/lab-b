package interfaccia;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import comune.CitizenData;
import comune.DoctorData;
import comune.EventiAvversi;
import comune.InfoCentriVaccinali;
import comune.InfoCittadini;
import comune.PrenotazioniVaccini;
import comune.RegistrazioniVaccinati;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

/**
 * Classe interfaccia del Server.
 * 
 * @author Luca
 */
public interface Server extends Remote {
	
	Integer subscribeToEvents(Client client) throws RemoteException;
	
	void unsubscribeToEvents(Integer clientId) throws RemoteException;
	
	/**
	 * Interfaccia per registrare il centro vaccinale.
	 * @param infoCentroVaccinale 	Le informazioni del centro vaccinale.
	 * @return						Un codice per gestire i vari casi di avviso ed errore.
	 * @throws RemoteException
	 */
	int registraCentroVaccinale(InfoCentriVaccinali infoCentroVaccinale) throws RemoteException;
	
	/**
	 * Interfaccia per registrare il cittadino.
	 * @param citizenData		Le informazioni del cittadino da registrare.
	 * @return					Un codice per gestire i vari casi di avviso ed errore.
	 * @throws RemoteException
	 */
	int registraCittadino(CitizenData citizenData) throws RemoteException;
	
	/**
	 * Interfaccia per registrare l'operatore sanitario.
	 * @param doctorData		Le informazioni dell'operatore sanitario da registrare.
	 * @return					Un codice per gestire i vari casi di avviso ed errore.
	 * @throws RemoteException
	 */
	int registraDottore(DoctorData doctorData) throws RemoteException;
	
	/**
	 * Interfaccia per gestire il login del cittadino che sta effettuando il login.
	 * @param username			Lo username del cittadino che sta effettuando il login.
	 * @param password			La password del cittadino che sta effettuando il login.
	 * @return					Un codice per gestire i vari casi di avviso ed errore.
	 * @throws RemoteException
	 */
	int loginCittadino(String username, String password) throws RemoteException;
	
	/**
	 * Interfaccia per gestire il login dell'operatore sanitario che sta effettuando il login.
	 * @param id				L'Id dell'operatore sanitario che sta effettuando il login.
	 * @param password			La password dell'operatore sanitario che sta effettuando il login.
	 * @return					Un codice per gestire i vari casi di avviso ed errore.
	 * @throws RemoteException
	 */
	int loginDottore(String id, String password) throws RemoteException;
	
	/**
	 * Interfaccia per gestire la ricerca del centro vaccinale.
	 * @param testo				Il testo di ricerca.
	 * @return					Una lista contenente tutti i dati dei centri vaccinali.
	 * @throws RemoteException
	 */
	List<InfoCentriVaccinali> cercaCentroVaccinale(String testo) throws RemoteException;
	
	/**
	 * Interfaccia per gestire il Cf del cittadino registrato in base all'username.
	 * @param username			L'username del cittadino registrato.
	 * @return					Il Cf del cittadino registrato.
	 * @throws RemoteException
	 */
	String ottieniCf(String username) throws RemoteException;
	
	/**
	 * Interfaccia per gestire la prenotazione della vaccinazione.
	 * @param prenotazioneVaccino	I dati di prenotazione.
	 * @return						Un codice per gestire i vari casi di avviso ed errore.
	 * @throws RemoteException
	 */
	int prenotaVaccinazione(PrenotazioniVaccini prenotazioneVaccino) throws RemoteException;
	
	/**
	 * Interfaccia per gestire la ricerca della prenotazione della vaccinazione.
	 * @param cf				Il Cf del cittadino che ha prenotato la vaccinazione.
	 * @return					I dati della vaccinazione per quel cittadino.
	 * @throws RemoteException
	 */
	PrenotazioniVaccini getPrenotazioneVaccinazione(String cf) throws RemoteException;
	
	/**
	 * Interfaccia per gestire il controllo dei dati prima della prenotazione.
	 * @param cf				Il Cf del cittadino registrato.
	 * @return					Un codice per gestire i vari casi di avviso ed errore.
	 * @throws RemoteException
	 */
	int controlloPreRegistrazioneEventoAvverso(String cf) throws RemoteException;
	
	/**
	 * Interfaccia per gestire l'Id vaccinazione dell'utente registrato
	 * @param cf				Il Cf del cittadino registrato.
	 * @return					L'Id vaccinazione del cittadino registrato.
	 * @throws RemoteException
	 */
	String ottieniIdVaccinazione(String cf) throws RemoteException;
	
	/**
	 * Interfaccia per gestire il numero di segnalazioni per un determinato centro vaccinale.
	 * @param nomeCentro		Il nome del centro vaccinale.
	 * @return					Il numero di segnalazioni di eventi avversi per quel determinato centro vaccinale.
	 * @throws RemoteException
	 */
	int ottieniNumSegnalazioni(String nomeCentro) throws RemoteException;
	
	/**
	 * Interfaccia per gestire le informazioni dei cittadini registrati
	 * @param cf				Il Cf del cittadino registrato.
	 * @return					Il nome e il cognome del cittadino.
	 * @throws RemoteException
	 */
	InfoCittadini getInfoCittadini(String cf) throws RemoteException;
	
	/**
	 * Interfaccia per gestire l'esistenza della prenotazione della vaccinazione.
	 * @param cf				Il Cf del cittadino che ha effettuato la prenotazione.
	 * @return					Se esiste o no la prenotazione.
	 * @throws RemoteException
	 */
	boolean existPrenotazioneOnDb(String cf) throws RemoteException;
	
	/**
	 * Interfaccia per gestire la vaccinazione di un cittadino.
	 * @param datiRegistrazione	I dati che il cittadino registrato ha inserito al momento della prenotazione.
	 * @return					Un codice per gestire i vari casi di avviso ed errore.
	 * @throws RemoteException
	 */
	public int registraVaccinato(RegistrazioniVaccinati datiRegistrazione) throws RemoteException;
	
	/**
	 * Interfaccia per gestire l'inserimento di eventi avversi.
	 * @param eventoAvverso		I dati di un gruppo di eventi aversi.
	 * @return					Un codice per gestire i vari casi di avviso ed errore.
	 * @throws RemoteException
	 */
	public int InserisciEventiAvversi(EventiAvversi eventoAvverso) throws RemoteException;
	
	/**
	 * Interfaccia per gestire l'ottenimento della severita' media per un determinato tipo di evento.
	 * @param nomeCentro		Il nome del centro vaccinale.
	 * @param evento			Il tipo di evento avverso.
	 * @return					La media dei valori dell'intensita' per quel determinato tipo di evento.
	 * @throws RemoteException
	 */
	public Double getImportanzaEvento(String nomeCentro, String evento) throws RemoteException;
	
	public int[] getStatisticheHomepage() throws RemoteException;
}
