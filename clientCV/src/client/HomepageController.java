package client;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import centrivaccinali.view.LoginDoctorView;
import cittadini.view.LoginCitizenView;
import cittadini.view.ShowInfoCentroVaccinaleCitizenView;
import comune.InfoCentriVaccinali;
import interfaccia.Client;
import interfaccia.Server;

public class HomepageController extends UnicastRemoteObject implements Client  {
	
	private HomepageView view;
	
	Server stub;
	
	int id;
	
	public HomepageController(HomepageView view) throws RemoteException {
		this.view = view;
		this.stub = ServerSingleton.getInstance();
		try {
			int[] statistiche = stub.getStatisticheHomepage();
			view.setStatistiche(statistiche[0], statistiche[1]);
		} catch (RemoteException ex) {
			ex.printStackTrace();
		}
		id = stub.subscribeToEvents(this);
	}

	@Override
	public void update(int[] statistiche) throws RemoteException {
		if(statistiche[0] != 0)
			view.updateStatisticheCentri(statistiche[0]);
		else if(statistiche[1] != 0)
				view.updateStatisticheVaccinati(statistiche[1]);
	}
	
	public void showInTableAction(String research, DefaultTableModel tableModel) {
		List<InfoCentriVaccinali> centriVaccinali = new ArrayList<>();
		
		if(research != null && research.trim().length() > 1)
			try {
				centriVaccinali = stub.cercaCentroVaccinale(research);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
	
		tableModel.setRowCount(0);
		
		for(int i=0; i<centriVaccinali.size(); i++) {
			String nome = centriVaccinali.get(i).getNomeCentro();
			String indirizzo = centriVaccinali.get(i).getIndirizzo();
			String tipo = centriVaccinali.get(i).getTipoCentro();
			
			Object[] infoData = {nome, indirizzo, tipo};
		
			tableModel.addRow(infoData);
		}
	}
	
	public void logInCitizenAction() {
		LoginCitizenView LogCitizen = new LoginCitizenView();
		try {
			stub.unsubscribeToEvents(id);
		} catch (RemoteException ex) {
			ex.printStackTrace();
		}
		view.deleteView();
	}
	
	public void logInDoctorAction() {
		LoginDoctorView LogDoctor = new LoginDoctorView();
		try {
			stub.unsubscribeToEvents(id);
		} catch (RemoteException ex) {
			ex.printStackTrace();
		}
		view.deleteView();
	}
	
	public void visualizzaInfoCentroVaccinaleAction(JTable table) {
		ShowInfoCentroVaccinaleCitizenView showInfoCentro = new ShowInfoCentroVaccinaleCitizenView();
		int index = table.getSelectedRow();
		TableModel model = table.getModel();
		
		InfoCentriVaccinali info = new InfoCentriVaccinali(model.getValueAt(index, 0).toString(), model.getValueAt(index, 1).toString(), model.getValueAt(index, 2).toString());

		showInfoCentro.infoNomeCentro.setText(info.getNomeCentro());
		showInfoCentro.infoIndirizzo.setText(info.getIndirizzo());
		showInfoCentro.infoTipoCentro.setText(info.getTipoCentro());
		
		try {
			int nSegnalazioni = stub.ottieniNumSegnalazioni(model.getValueAt(index, 0).toString());
			showInfoCentro.numSegnalazioni.setText(Integer.toString(nSegnalazioni));
			
			String[] tipoEvento = {"Mal di testa",
					"Febbre",
					"Dolori muscolari e articolari",
					"Linfoadenopatia",
					"Tachicardia",
					"Crisi ipertensiva"
			};
			
			List<Double> importanzaEventi = new ArrayList<>();
			
			for(int i=0; i<tipoEvento.length; i++) {
				Double importanza = stub.getImportanzaEvento(info.getNomeCentro(), tipoEvento[i]);
				importanzaEventi.add(Math.round(importanza*100.0)/100.0);
			}
			
			for(int i=0; i<importanzaEventi.size(); i++) {
				Object[] mediaEvento = {tipoEvento[i], importanzaEventi.get(i)};
				
				showInfoCentro.tableModel.addRow(mediaEvento);
			}
				
			
		} catch (RemoteException ex) {
			ex.printStackTrace();
		}
	}
}
