package client;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import interfaccia.Server;

public class ServerSingleton {
	
	private static Server instance;
	
	private ServerSingleton () {}
	
	public static Server getInstance() {
		if(instance == null) {
			try {
				Registry registry = LocateRegistry.getRegistry(1099);
				instance = (Server)registry.lookup("serverCV");
			} catch (Exception ex) {
				System.out.println(ex);
			}
		}
			
		return instance;
	}
	
	
}
