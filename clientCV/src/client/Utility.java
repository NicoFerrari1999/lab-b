package client;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

/**
 * Classe contenente metodi di utilit� per il package clientCV.  
 * 
 * @author Luca
 */
public class Utility {
	
	/**
	 * Visualizza un pop-up di errore.
	 * @param titolo	Il titolo dell'errore.
	 * @param messaggio Il messaggio di errore.
	 */
	public static void mostraErrorePopUp(String titolo, String messaggio)
    {
        JOptionPane.showMessageDialog(null, messaggio, titolo, JOptionPane.ERROR_MESSAGE);
    }
	
	/**
	 * Visualizza pop-up di informazione.
	 * @param intestazione 	Il titolo dell'informazione.
	 * @param messaggio		Il messaggio dell'informazione.
	 */
	public static void mostraInformazionePopUp(String titolo,String messaggio) {
        JOptionPane.showMessageDialog(null, messaggio, titolo, JOptionPane.INFORMATION_MESSAGE);
    }

	/**
	 * Crypta la stringa passata in input tramite algoritmo MD5.
	 * @param input	La stringa da cifrare.
	 * @return		La stringa cifrata.
	 */
	public static String encryptMD5 (String input){
        String hash = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] arrayMessageDigest = md.digest(input.getBytes());
            BigInteger numero = new BigInteger(1, arrayMessageDigest);
            hash = numero.toString(16);
            while (hash.length() < 32) {
                hash = "0" + hash;
            }
        }
        catch(Exception ex){} 
        return hash;
    }
}
