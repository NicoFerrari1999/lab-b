package client;

import java.rmi.RemoteException;

import interfaccia.Server;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

/**
 * Classe che avvia la comunicazione col server. 
 * 
 * @author Luca
 */
public class ClientStart {

	public static void start() {
		Server stub = ServerSingleton.getInstance();
		HomepageView home = new HomepageView();
	}
}