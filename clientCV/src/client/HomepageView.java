package client;

import java.awt.Color;
import java.awt.event.*;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import centrivaccinali.view.LoginDoctorView;
import cittadini.view.LoginCitizenView;
import cittadini.view.ShowInfoCentroVaccinaleCitizenView;
import comune.InfoCentriVaccinali;
import interfaccia.Server;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

/**
 * Classe che permette agli utenti non registrati di effettuare il login e ricercare i centri vaccinali. 
 * 
 * @author Luca
 */
public class HomepageView extends JFrame {
	
	private static final int WIDTH 		= 800;
	private static final int HEIGHT 	= 600;
	private static final String FIND	= "Ricerca per Nome, Comune, Provincia o Vaccino";
	
	boolean resetText = true;

	JPanel 				panel_Homepage;
	JFrame 				frame_Homepage;
	JLabel 				log_Citizen;
	JLabel 				log_Doctor;
	ImageIcon 			searchPicture;
	JButton 			search;
	JTextField 			find;
	JTable 				table;
	DefaultTableModel 	tableModel;
	HomepageController 	controller;
	JLabel 				label_CountCentriVaccinali;
	JLabel 				label_CountCittadiniVaccinati;
	int 				countCentriVaccinali;
	int					countCittadiniVaccinati;

	public HomepageView() {
		panel_Homepage = new JPanel();
		frame_Homepage = new JFrame();
		
		try {
			this.controller = new HomepageController(this);
		} catch (RemoteException ex) {
			ex.printStackTrace();
		}

		frame_Homepage.setTitle("CENTRI VACCINALI");
		frame_Homepage.setSize(WIDTH, HEIGHT);
		frame_Homepage.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame_Homepage.setResizable(false);
		frame_Homepage.setLocationRelativeTo(null);
		frame_Homepage.add(panel_Homepage);
		
		panel_Homepage.setLayout(null);
		
		log_Citizen = new JLabel("Login cittadino");
		log_Citizen.setBounds(650, 20, 100, 25);
		panel_Homepage.add(log_Citizen);
		
		log_Doctor = new JLabel("Login dottore");
		log_Doctor.setBounds(650, 40, 100, 25);
		panel_Homepage.add(log_Doctor);
		
		tableModel = new DefaultTableModel(new String[] {"Nome centro", "Indirizzo", "Tipo Centro"}, 0);
		table = new JTable(tableModel) {
			public boolean editCellAt(int row, int column, java.util.EventObject e) {
	            return false;
			}
		};
		table.getTableHeader().setReorderingAllowed(false);
		
		searchPicture = new ImageIcon("ext-img//Search.png");
		search = new JButton(searchPicture);
		search.setBounds(171, 100, 29, 29);
		search.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String researchText = find.getText();
				controller.showInTableAction(researchText, tableModel);
			}
		});
		
		table.setRowSelectionAllowed(false);
		JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setBounds(1, 150, 798, 200);
		panel_Homepage.add(scrollPane);
		panel_Homepage.add(search);
		
		find = new JTextField(20);
		find.setText(FIND);
		find.setBounds(200, 100, 400, 29);
		panel_Homepage.add(find);
		
		label_CountCentriVaccinali = new JLabel("Numero di centri vaccinali disponibili: " +countCentriVaccinali);
		label_CountCentriVaccinali.setBounds(0, 380, 800, 25);
		label_CountCentriVaccinali.setHorizontalAlignment(JLabel.CENTER);
		label_CountCentriVaccinali.setVerticalAlignment(JLabel.CENTER);
		panel_Homepage.add(label_CountCentriVaccinali);
		
		label_CountCittadiniVaccinati = new JLabel("Numero di cittadini vaccinati: " +countCittadiniVaccinati);
		label_CountCittadiniVaccinati.setBounds(0, 410, 800, 25);
		label_CountCittadiniVaccinati.setHorizontalAlignment(JLabel.CENTER);
		label_CountCittadiniVaccinati.setVerticalAlignment(JLabel.CENTER);
		panel_Homepage.add(label_CountCittadiniVaccinati);
		
		frame_Homepage.setVisible(true);
		
		log_Citizen.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				controller.logInCitizenAction();
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				log_Citizen.setForeground(Color.RED);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				log_Citizen.setForeground(Color.BLACK);
			}
			
			
		});
		
		log_Doctor.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				controller.logInDoctorAction();
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				log_Doctor.setForeground(Color.RED);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				log_Doctor.setForeground(Color.BLACK);
			}
			
		});
		
		find.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if(resetText) {
					find.setText("");
					resetText = false;
				}
			}
		});
		
		/**
		 * Metodo che, interfacciandosi col server ottiene i valori anonimi degli eventi avversi per poi riportarli in una tabella riassuntiva.
		 */
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				controller.visualizzaInfoCentroVaccinaleAction(table);
			}
		});
	}
		
	public void deleteView() {
		frame_Homepage.setVisible(false);
		frame_Homepage.dispose();
	}
	
	public void setStatistiche(int countCentriVaccinali, int countCittadiniVaccinati) {
		this.countCentriVaccinali = countCentriVaccinali;
		this.countCittadiniVaccinati = countCittadiniVaccinati;
		panel_Homepage.validate();
		panel_Homepage.repaint();
	}
	
	public void updateStatisticheCentri(int nuoviCentri) {
		countCentriVaccinali += nuoviCentri;
		label_CountCentriVaccinali.setText("Numero di centri vaccinali disponibili: " +countCentriVaccinali);
		panel_Homepage.validate();
		panel_Homepage.repaint();
	}
	
	public void updateStatisticheVaccinati(int nuoviVaccinati) {
		countCittadiniVaccinati += nuoviVaccinati;
		label_CountCittadiniVaccinati.setText("Numero di cittadini vaccinati: " +countCittadiniVaccinati);
		panel_Homepage.validate();
		panel_Homepage.repaint();
	}
	
}
