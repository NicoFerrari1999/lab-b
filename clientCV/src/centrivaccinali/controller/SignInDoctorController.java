package centrivaccinali.controller;

import java.awt.event.KeyEvent;
import java.rmi.RemoteException;

import centrivaccinali.view.SignInDoctorView;
import client.ServerSingleton;
import client.Utility;
import comune.DoctorData;
import interfaccia.Server;

public class SignInDoctorController {

	private SignInDoctorView view;
	
	private Server stub;
	
	public SignInDoctorController(SignInDoctorView view) {
		this.view = view;
		this.stub = ServerSingleton.getInstance();;
	}
	
	public void resetAction() {
		view.reset();
	}
	
	public void checkChar(KeyEvent e) {
		int ev = e.getKeyChar();
		if(!((ev >= 48 && ev <= 57) || ev == 8)) {
			Utility.mostraErrorePopUp("ERRORE", "Inserire solo numeri");
			e.consume();
			return;
		}
	}
	
	public void saveDoctorAction(String nome, String cognome, String id, String email, char[] pass1, char[] pass2) {
		String pw = String.valueOf(pass1);
		String pw2 = String.valueOf(pass2);

		if(nome.equals("") || cognome.equals("") || id.equals("") || email.equals("") || pw.equals("")) {
			Utility.mostraErrorePopUp("ERRORE", "Compila tutti i campi");
	        return;
		}
	    
		if(id.length() != 8) {
			Utility.mostraErrorePopUp("ERRORE", "L'Id deve essere lungo 8 cifre");
	        return;
		}
		
	    if(!email.contains("@") || email.lastIndexOf("@") == (email.length()-1)) {
	    	Utility.mostraErrorePopUp("ERRORE", "Inserisci una email valida");
	        return;
	    }
	    
	    if(!pw.equals(pw2)) {
	    	Utility.mostraErrorePopUp("ERRORE", "Le due password non coincidono");
	        return;
	    }
	        
	    pw = Utility.encryptMD5(pw);
	    DoctorData tempDoctor = new DoctorData(nome, cognome, id, email, pw);
	       
	    try {
	    	int risultato = stub.registraDottore(tempDoctor);
	    	if(risultato == 1) {
	    		Utility.mostraInformazionePopUp("CONGRATULAZIONI", "Registrazione avvenuta con successo");
	    		view.deleteView();
	    	}
	    	if(risultato == 2) {
	    		Utility.mostraErrorePopUp("ERRORE", "L'Id inserito � gi� stato utilizzato");
	    	}
	    } catch (RemoteException ex) {
	    	ex.printStackTrace();
	    }
	}
}
