package centrivaccinali.controller;

import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import centrivaccinali.view.AddVaccinatoView;
import client.ServerSingleton;
import client.Utility;
import comune.InfoCittadini;
import comune.PrenotazioniVaccini;
import comune.RegistrazioniVaccinati;
import interfaccia.Server;

public class AddVaccinatoController {
	
	private AddVaccinatoView view;
	
	Server stub;
	
	public AddVaccinatoController(AddVaccinatoView view) {
		this.view = view;
		stub = ServerSingleton.getInstance();
	}
	
	public void resetAction() {
		view.reset();
	}
	
	public void obtainDataAction(String cf) {
		PrenotazioniVaccini infoPrenotazione = null;
		InfoCittadini infoCittadino = null;
		
		if(cf.equals("")) {
			Utility.mostraErrorePopUp("ERRORE", "Il campo CF non pu� essere vuoto");
			return;
		}

		if(cf.length() != 16) {
			Utility.mostraErrorePopUp("ERRORE", "Il campo CF pu� contenere al massimo 16 caratteri");
			return;
		}
		
		try {
			if(!stub.existPrenotazioneOnDb(cf)) {
				Utility.mostraErrorePopUp("ERRORE", "Il CF inserito non compare nella lista degli utenti prenotati");
				return;
			}
		} catch (RemoteException ex) {
			ex.printStackTrace();
		}
		
		try {
			infoPrenotazione = stub.getPrenotazioneVaccinazione(cf);
		} catch (RemoteException ex) {
			ex.printStackTrace();
		}
		
		try {
			infoCittadino = stub.getInfoCittadini(cf);
		} catch (RemoteException ex) {
			ex.printStackTrace();
		}
		
		view.showDatiVaccinato(infoPrenotazione, infoCittadino);
	}
	
	public void registraVaccinatoAction(String nomeCentro, String cf, String data, String tipoVaccino, String idVaccinazione) {
		java.util.Date oldData = null;
		String newData = null;
		int id = Integer.parseInt(idVaccinazione);
		
		if(tipoVaccino.equals("")) {
			Utility.mostraErrorePopUp("ERRORE", "Inserire un tipo di vaccino");
			return;
		}
		
		if(idVaccinazione.length() == 0) {
			Utility.mostraErrorePopUp("ERRORE", "L'Id vaccinazione non pu� essere vuoto");
			return;
		}
		
		// Converte la data dal formato dd-MM-yyyy al formato yyyy-MM-dd per rendere possibile 
		// l'inserimento della data scelta dal calendario nel DB
		SimpleDateFormat oldDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat newDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String newFormat = "yyyy-MM-dd";
		
		try {
			oldData = oldDateFormat.parse(data);
			oldDateFormat.applyPattern(newFormat);
			newData = oldDateFormat.format(oldData);
			oldData = newDateFormat.parse(newData);
		} catch (ParseException ex) {
			ex.printStackTrace();
		}
		
		java.sql.Date dataVaccino = new java.sql.Date(oldData.getTime());
		
		RegistrazioniVaccinati datiRegistrazioni = new RegistrazioniVaccinati(nomeCentro, cf, dataVaccino, tipoVaccino, id);
		
		try {
			int risultato = stub.registraVaccinato(datiRegistrazioni);
			if(risultato == 3) {
				Utility.mostraErrorePopUp("ERRORE", "Questo CF risulta appartenere ad un cittadino gi� vaccinato");
				return;
			}
			if(risultato == 2) {
				Utility.mostraErrorePopUp("ERRORE", "Questo id � gi� stato usato per un altro cittadino vaccinato");
				return;
			}
			if(risultato == 1) {
				Utility.mostraInformazionePopUp("CONGRATULAZIONI", "Vaccinato aggiunto con successo");
				view.deleteView();
			}
		} catch (RemoteException ex) {
			ex.printStackTrace();
		}
	}
}
