package centrivaccinali.controller;

import centrivaccinali.view.AddCentroVaccinaleView;
import centrivaccinali.view.AddVaccinatoView;
import centrivaccinali.view.LoggedDoctorView;
import client.HomepageView;
import client.ServerSingleton;
import client.Utility;
import interfaccia.Server;

public class LoggedDoctorController {
	
	private LoggedDoctorView view;
	
	private Server stub;
	
	public LoggedDoctorController(LoggedDoctorView view) {
		this.view = view;
		stub = ServerSingleton.getInstance();
	}
	
	public void logOutAction() {
		Utility.mostraInformazionePopUp("", "Logout avvenuto con successo, arrivederci");
		HomepageView home = new HomepageView();
		view.deleteView();
	}
	
	public void createCentroVaccinaleAction() {
		AddCentroVaccinaleView addCentroVaccinaleView = new AddCentroVaccinaleView();
	}
	
	public void aggiungiVaccinatiAction() {
		AddVaccinatoView addVaccinato = new AddVaccinatoView();
	}
}
