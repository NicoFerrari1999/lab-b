package centrivaccinali.controller;

import java.rmi.RemoteException;

import centrivaccinali.view.LoggedDoctorView;
import centrivaccinali.view.LoginDoctorView;
import centrivaccinali.view.SignInDoctorView;
import client.HomepageView;
import client.ServerSingleton;
import client.Utility;
import interfaccia.Server;

public class LoginDoctorController {
	
	private LoginDoctorView view;
	
	private Server stub;
	
	public LoginDoctorController(LoginDoctorView view) {
		this.view = view;
		this.stub = ServerSingleton.getInstance();
	}
	
	public void checkDoctorCredentialsAction(String id, char[] password) {
		String typedPassword = String.valueOf(password);
		typedPassword = Utility.encryptMD5(typedPassword);
		
		if(id.equals("") || typedPassword.equals("")) {
            Utility.mostraErrorePopUp("ERRORE", "Compila tutti i campi.");
            return;
        }
		
		try {
			
			int risultato = stub.loginDottore(id, typedPassword);
			if(risultato == 1) {
				LoggedDoctorView loggedDoctor = new LoggedDoctorView();
				loggedDoctor.id.setText(id);
				view.deleteView();
			}
			if(risultato == 2) {
				Utility.mostraErrorePopUp("ERRORE", "Id i	nesistente, registrati");
				return;
			}
			if(risultato == 3) {
				Utility.mostraErrorePopUp("Errore", "Id medico o Password non corretti");
				return;
			}
		} catch (RemoteException ex) {
			ex.printStackTrace();
		}
	}
	
	public void backAction() {
		HomepageView homepage = new HomepageView();
		view.deleteView();
	}
	
	public void singInAction() {
		SignInDoctorView signInDoctor = new SignInDoctorView();
	}
}
