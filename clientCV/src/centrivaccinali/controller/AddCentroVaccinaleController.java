package centrivaccinali.controller;

import centrivaccinali.view.AddCentroVaccinaleView;
import client.ServerSingleton;
import client.Utility;
import comune.InfoCentriVaccinali;
import interfaccia.Server;

public class AddCentroVaccinaleController {
	
	private AddCentroVaccinaleView view;
	
	Server stub;
	
	public AddCentroVaccinaleController(AddCentroVaccinaleView view) {
		this.view = view;
		stub = ServerSingleton.getInstance();
	}
	
	public void resetAction() {
		view.reset();
	}
	
	/**
	 * Metodo che controlla se i campi inseriti sono vuoti e invia al server i dati necessari per la creazione del centro vaccinale.
	 * In base al codice restituito dal server puo' mostrare, o un messaggio d'errore o uno di avvenuta creazione. 
	 */
	public void registraCentroVaccinaleAction(String nomeCentroVaccinale, String indirizzoCentroVaccinale, String tipologiaCentroVaccinale) {
		if(nomeCentroVaccinale.equals("") || indirizzoCentroVaccinale.equals("") || tipologiaCentroVaccinale.equals("")) {
			Utility.mostraErrorePopUp("ERRORE", "Compila tutti i campi");
			return;
		}
		
		InfoCentriVaccinali infoCentroVaccinale = new InfoCentriVaccinali(nomeCentroVaccinale, indirizzoCentroVaccinale, tipologiaCentroVaccinale);
		
		try {
			int risultato = stub.registraCentroVaccinale(infoCentroVaccinale);
			if(risultato == 2) {
				Utility.mostraErrorePopUp("ERRORE", "Centro gi� esistente");
				return;
			}
			if(risultato == 1) {
				Utility.mostraInformazionePopUp("CONGRATULAZIONI", "Centro aggiunto con successo");
				view.deleteView();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
