package centrivaccinali.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;
import java.rmi.RemoteException;

import javax.swing.*;

import centrivaccinali.controller.LoginDoctorController;
import client.HomepageView;
import client.Utility;
import interfaccia.Server;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

/**
 * Classe che permette il login degli operatori sanitari.
 * 
 * @author Nico
 */
public class LoginDoctorView extends JFrame {
	
	private static final int WIDTH 	= 300;
	private static final int HEIGHT	= 300;
	
	private JFrame 			frame_LoginDoctor;
	private JPanel 			panel_LoginDoctor;
	private JTextField 		tf_Id;
	private JPasswordField 	pf_Password;
	private LoginDoctorController controller;
	
	public LoginDoctorView() {
		controller = new LoginDoctorController(this);
		
		frame_LoginDoctor = new JFrame();
		panel_LoginDoctor = new JPanel();
		frame_LoginDoctor.setTitle("CENTRI VACCINALI - LOGIN CITTADINO");
		frame_LoginDoctor.setSize(WIDTH, HEIGHT);
		frame_LoginDoctor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame_LoginDoctor.setResizable(false);
		frame_LoginDoctor.setLocationRelativeTo(null);
		frame_LoginDoctor.add(panel_LoginDoctor);
		
		panel_LoginDoctor.setLayout(null);
		
		JLabel back = new JLabel("BACK");
		back.setBounds(10, 10, 40, 25);
		panel_LoginDoctor.add(back);
		
		JLabel username = new JLabel("Id medico");
		username.setBounds(10, 50, 60, 25);
		panel_LoginDoctor.add(username);
		
		tf_Id = new JTextField(20);
		tf_Id.setBounds(85, 50, 180, 25);
		panel_LoginDoctor.add(tf_Id);
		
		JLabel password = new JLabel("Password");
		password.setBounds(10, 100, 60, 25);
		panel_LoginDoctor.add(password);
		
		pf_Password = new JPasswordField();
		pf_Password.setBounds(85, 100, 180, 25);
		panel_LoginDoctor.add(pf_Password);
		
		JLabel signIn = new JLabel("Nuovo medico? Registrati ora");
		signIn.setFont(new Font("Verdana", Font.ITALIC, 10));
		signIn.setBounds(100, 130, 160, 15);
		panel_LoginDoctor.add(signIn);
		
		JButton button_Login = new JButton("LOGIN");
		button_Login.setBounds(105, 160, 90, 20);
		panel_LoginDoctor.add(button_Login);
		
		/**
		 * Metodo che, dopo aver inviato i dati al server, ed aver effettuato vari controlli, permette di effettuare il login degli operatori sanitari.
		 */
		button_Login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String id = tf_Id.getText();
				char[] password = pf_Password.getPassword();
				controller.checkDoctorCredentialsAction(id, password);
			}
		});
		
		frame_LoginDoctor.setVisible(true);
		
		back.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				controller.backAction();
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				back.setForeground(Color.RED);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				back.setForeground(Color.BLACK);
			}
			
			
		});
	
		signIn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				controller.singInAction();
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				signIn.setForeground(Color.BLUE);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				signIn.setForeground(Color.BLACK);
			}
			
		});
	}
	
	public void deleteView() {
		frame_LoginDoctor.setVisible(false);
		frame_LoginDoctor.dispose();
	}
	
}
