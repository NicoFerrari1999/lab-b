package centrivaccinali.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.rmi.RemoteException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import centrivaccinali.controller.SignInDoctorController;
import client.Utility;
import comune.DoctorData;
import interfaccia.Server;


/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

/**
 * Classe che permette la registrazione degli operatori sanitari.
 * 
 * @author Nico
 */
public class SignInDoctorView {
	
	private static final int WIDTH 	= 380;
	private static final int HEIGHT = 400;
	
	private JFrame 			frame_SignInDoctor;
	private JPanel 			panel_SignInDoctor;
	private JTextField 		tf_Name;
	private JTextField 		tf_Surname;
	private JTextField 		tf_DoctorId;
	private JTextField 		tf_Email;
	private JTextField 		tf_Username;
	private JPasswordField 	pf_Password;
	private JPasswordField	pf_Password2;
	private SignInDoctorController controller;
	
	public SignInDoctorView() {
		controller = new SignInDoctorController(this);
		
		frame_SignInDoctor = new JFrame();
		panel_SignInDoctor = new JPanel();
		frame_SignInDoctor.setTitle("CENTRI VACCINALI - SIGN-IN");
		frame_SignInDoctor.setSize(WIDTH, HEIGHT);
		frame_SignInDoctor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame_SignInDoctor.setResizable(false);
		frame_SignInDoctor.setLocationRelativeTo(null);
		frame_SignInDoctor.add(panel_SignInDoctor);
			
		panel_SignInDoctor.setLayout(null);
		
		JLabel label_Name = new JLabel("Nome");
		label_Name.setBounds(10, 50, 60, 25);
		panel_SignInDoctor.add(label_Name);
		
		tf_Name = new JTextField(1);
		tf_Name.setBounds(150, 50, 200, 25);
		panel_SignInDoctor.add(tf_Name);
		
		JLabel label_Surname = new JLabel("Cognome");
		label_Surname.setBounds(10, 80, 60, 25);
		panel_SignInDoctor.add(label_Surname);
		
		tf_Surname = new JTextField(1);
		tf_Surname.setBounds(150, 80, 200, 25);
		panel_SignInDoctor.add(tf_Surname);
		
		JLabel label_DoctorId = new JLabel("Id medico");
		label_DoctorId.setBounds(10, 110, 60, 25);
		panel_SignInDoctor.add(label_DoctorId);
		
		tf_DoctorId = new JTextField(1);
		tf_DoctorId.setBounds(150, 110, 200, 25);
		panel_SignInDoctor.add(tf_DoctorId);
		
		JLabel label_Email = new JLabel("Email");
		label_Email.setBounds(10, 140, 60, 25);
		panel_SignInDoctor.add(label_Email);
		
		tf_Email = new JTextField(1);
		tf_Email.setBounds(150, 140, 200, 25);
		panel_SignInDoctor.add(tf_Email);
		
		JLabel label_Password = new JLabel("Password");
		label_Password.setBounds(10, 170, 60, 25);
		panel_SignInDoctor.add(label_Password);
	
		pf_Password = new JPasswordField();
		pf_Password.setBounds(150, 170, 200, 25);
		panel_SignInDoctor.add(pf_Password);
			
		JLabel secondPassword = new JLabel("Conferma password");
		secondPassword.setBounds(10, 200, 120, 25);
		panel_SignInDoctor.add(secondPassword);
		
		pf_Password2 = new JPasswordField();
		pf_Password2.setBounds(150, 200, 200, 25);
		panel_SignInDoctor.add(pf_Password2);
		
		JButton button_SignIn = new JButton("SIGN-IN");
		button_SignIn.setBounds(50, 250, 80, 30);
		panel_SignInDoctor.add(button_SignIn);
		
		/**
		 * Metodo che, interfacciandosi con il server, salva le informazioni relative al dottore dopo aver eseguito gli opportuni controlli.
		 */
		button_SignIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nome = tf_Name.getText();
				String cognome = tf_Surname.getText();
				String id = tf_DoctorId.getText();
				String email = tf_Email.getText();
				char[] password1 = pf_Password.getPassword();
				char[] password2 = pf_Password2.getPassword();
				controller.saveDoctorAction(nome, cognome, id, email, password1, password2);
			}
		});
			
		JButton button_Reset = new JButton("RESET");
		button_Reset.setBounds(200, 250, 75, 30);
		panel_SignInDoctor.add(button_Reset);
		
		button_Reset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.resetAction();
			}
		});
			
		frame_SignInDoctor.setVisible(true);
		
		/**
		 * Controlla il codice del carattere digitato.
		 */
		tf_DoctorId.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
				controller.checkChar(e);
			}

			@Override
			public void keyPressed(KeyEvent e) {}
			
			@Override
			public void keyReleased(KeyEvent e) {}
		});
	}
		
	/**
	 * Metodo che resetta i valori nei campi.
	 */
	public void reset() {
		tf_Name.setText("");
		tf_Surname.setText("");
		tf_DoctorId.setText("");
		tf_Email.setText("");
		tf_Username.setText("");
		pf_Password.setText("");
		pf_Password2.setText("");
	}

	public void deleteView() {
		frame_SignInDoctor.setVisible(false);
	    frame_SignInDoctor.dispose();
	}
}
