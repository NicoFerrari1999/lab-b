package centrivaccinali.view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import centrivaccinali.controller.AddVaccinatoController;
import client.Utility;
import comune.InfoCittadini;
import comune.PrenotazioniVaccini;
import comune.RegistrazioniVaccinati;
import interfaccia.Server;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

/**
 * Classe che permette all'operatore sanitario di registrare un nuovo vaccinato.
 * 
 * @author Nico
 */
public class AddVaccinatoView extends JFrame {
	
	private static final int WIDTH 	= 400;
	private static final int HEIGHT	= 650;
	
	private JFrame frame_AddVaccinatoGUI;
	private JPanel panel_AddVaccinatoGUI;
	private JLabel label_InfoNomeCentro;
	private JLabel label_NomeCittadino;
	private JLabel label_CognomeCittadino;
	private JTextField tf_Cf;
	private JLabel label_DataVaccino;
	private JComboBox cb_TipoVaccino;
	private JTextField tf_IdVaccinazione;
	private JButton obtainDatas;
	private JButton registraVaccinato;
	private AddVaccinatoController controller;
	
	public AddVaccinatoView() {
		controller = new AddVaccinatoController(this);
		
		frame_AddVaccinatoGUI = new JFrame();
		panel_AddVaccinatoGUI = new JPanel();
		
		frame_AddVaccinatoGUI.setTitle("INSERIMENTO VACCINATO");
		frame_AddVaccinatoGUI.setSize(WIDTH, HEIGHT);
		frame_AddVaccinatoGUI.setResizable(false);
		frame_AddVaccinatoGUI.setLocationRelativeTo(null);
		frame_AddVaccinatoGUI.add(panel_AddVaccinatoGUI);
		
		panel_AddVaccinatoGUI.setLayout(null);
		panel_AddVaccinatoGUI.setBackground(Color.WHITE);
		
		JLabel titolo = new JLabel("DATI ANAGRAFICI CITTADINO");
		titolo.setForeground(Color.RED);
		titolo.setBounds(0, 15, 400, 25);
		titolo.setHorizontalAlignment(JLabel.CENTER);
		titolo.setVerticalAlignment(JLabel.CENTER);
		panel_AddVaccinatoGUI.add(titolo);
		
		JLabel nomeCentro = new JLabel("Nome centro: ");
		nomeCentro.setForeground(Color.RED);
		nomeCentro.setBounds(0, 50, 400, 25);
		nomeCentro.setHorizontalAlignment(JLabel.CENTER);
		nomeCentro.setVerticalAlignment(JLabel.CENTER);
		panel_AddVaccinatoGUI.add(nomeCentro);
		
		label_InfoNomeCentro = new JLabel("");
		label_InfoNomeCentro.setBounds(0, 80, 400, 25);
		label_InfoNomeCentro.setHorizontalAlignment(JLabel.CENTER);
		label_InfoNomeCentro.setVerticalAlignment(JLabel.CENTER);
		panel_AddVaccinatoGUI.add(label_InfoNomeCentro);
		
		JLabel nomeCittadino = new JLabel("Nome: ");
		nomeCittadino.setForeground(Color.RED);
		nomeCittadino.setBounds(0, 110, 400, 25);
		nomeCittadino.setHorizontalAlignment(JLabel.CENTER);
		nomeCittadino.setVerticalAlignment(JLabel.CENTER);
		panel_AddVaccinatoGUI.add(nomeCittadino);
		
		label_NomeCittadino = new JLabel("");
		label_NomeCittadino.setBounds(0, 140, 400, 25);
		label_NomeCittadino.setHorizontalAlignment(JLabel.CENTER);
		label_NomeCittadino.setVerticalAlignment(JLabel.CENTER);
		panel_AddVaccinatoGUI.add(label_NomeCittadino);

		JLabel cognomeCittadino = new JLabel("Cognome: ");
		cognomeCittadino.setForeground(Color.RED);
		cognomeCittadino.setBounds(0, 170, 400, 25);
		cognomeCittadino.setHorizontalAlignment(JLabel.CENTER);
		cognomeCittadino.setVerticalAlignment(JLabel.CENTER);
		panel_AddVaccinatoGUI.add(cognomeCittadino);

		label_CognomeCittadino = new JLabel("");
		label_CognomeCittadino.setBounds(0, 200, 400, 25);
		label_CognomeCittadino.setHorizontalAlignment(JLabel.CENTER);
		label_CognomeCittadino.setVerticalAlignment(JLabel.CENTER);
		panel_AddVaccinatoGUI.add(label_CognomeCittadino);

		JLabel cfCittadino = new JLabel("CF: ");
		cfCittadino.setForeground(Color.RED);
		cfCittadino.setBounds(0, 230, 400, 25);
		cfCittadino.setHorizontalAlignment(JLabel.CENTER);
		cfCittadino.setVerticalAlignment(JLabel.CENTER);
		panel_AddVaccinatoGUI.add(cfCittadino);

		tf_Cf = new JTextField(1);
		tf_Cf.setBounds(100, 260, 200, 25);
		panel_AddVaccinatoGUI.add(tf_Cf);

		JLabel dataVaccino = new JLabel("Data vaccinazione: ");
		dataVaccino.setForeground(Color.RED);
		dataVaccino.setBounds(0, 290, 400, 25);
		dataVaccino.setHorizontalAlignment(JLabel.CENTER);
		dataVaccino.setVerticalAlignment(JLabel.CENTER);
		panel_AddVaccinatoGUI.add(dataVaccino);

		label_DataVaccino = new JLabel("");
		label_DataVaccino.setBounds(0, 320, 400, 25);
		label_DataVaccino.setHorizontalAlignment(JLabel.CENTER);
		label_DataVaccino.setVerticalAlignment(JLabel.CENTER);
		panel_AddVaccinatoGUI.add(label_DataVaccino);

		JLabel tipoVaccino = new JLabel("Tipo vaccino: ");
		tipoVaccino.setForeground(Color.RED);
		tipoVaccino.setBounds(0, 350, 400, 25);
		tipoVaccino.setHorizontalAlignment(JLabel.CENTER);
		tipoVaccino.setVerticalAlignment(JLabel.CENTER);
		panel_AddVaccinatoGUI.add(tipoVaccino);

		String[] vaccini = {"", "Pfizer", "AstraZeneca", "Moderna", "J&J"};
		cb_TipoVaccino = new JComboBox(vaccini);
		cb_TipoVaccino.setBounds(100, 380, 200, 25);
		panel_AddVaccinatoGUI.add(cb_TipoVaccino);
		cb_TipoVaccino.setEnabled(false);

		JLabel idVaccinazione = new JLabel("ID vaccinazione: ");
		idVaccinazione.setForeground(Color.RED);
		idVaccinazione.setBounds(0, 410, 400, 25);
		idVaccinazione.setHorizontalAlignment(JLabel.CENTER);
		idVaccinazione.setVerticalAlignment(JLabel.CENTER);
		panel_AddVaccinatoGUI.add(idVaccinazione);

		tf_IdVaccinazione = new JTextField(1);
		tf_IdVaccinazione.setBounds(100, 440, 200, 25);
		panel_AddVaccinatoGUI.add(tf_IdVaccinazione);
		tf_IdVaccinazione.setEnabled(false);

		obtainDatas = new JButton("OTTIENI DATI");
		obtainDatas.setBounds(100, 500, 200, 25);
		panel_AddVaccinatoGUI.add(obtainDatas);
		
		/**
		 * All'atto della pressione del bottone vengono eseguiti i vari controlli.
		 */
		obtainDatas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String cf = tf_Cf.getText();
				controller.obtainDataAction(cf);
			}
		});

		JButton reset = new JButton("RESET");
		reset.setBounds(100, 540, 200, 25);
		panel_AddVaccinatoGUI.add(reset);
		
		reset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.resetAction();
			}
		});

		registraVaccinato = new JButton("REGISTRA VACCINATO");
		registraVaccinato.setBounds(100, 580, 200, 25);
		
		/**
		 * Metodo che, dopo aver eseguito i vari controlli, invia al server i dati del cittadino che si � appena vaccinato. 
		 */
		registraVaccinato.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nomeCentro = label_InfoNomeCentro.getText();
				String cf = tf_Cf.getText();
				String data = label_DataVaccino.getText(); 
				String tipoVaccino = cb_TipoVaccino.getSelectedItem().toString();
				String idVaccinazione = tf_IdVaccinazione.getText();
				controller.registraVaccinatoAction(nomeCentro, cf, data, tipoVaccino, idVaccinazione);
			}
		});
		
		frame_AddVaccinatoGUI.setVisible(true);
	}
	
	/**
	 * Metodo che resetta i valori in tutti i campi.
	 */
	public void reset() {
		label_InfoNomeCentro.setText("");
		label_NomeCittadino.setText("");
		label_CognomeCittadino.setText("");
		tf_Cf.setText("");
		tf_Cf.setEditable(true);
		label_DataVaccino.setText("");
		cb_TipoVaccino.setSelectedIndex(0);
		tf_IdVaccinazione.setText("");
		obtainDatas.setEnabled(true);;
		panel_AddVaccinatoGUI.remove(registraVaccinato);
		panel_AddVaccinatoGUI.validate();
		panel_AddVaccinatoGUI.repaint();
	}
	
	/**
	 * Metodo che, dato il CF, ottiene dal server i dati della prenotazione e i dati del cittadino stesso 
	 * @param cf Il codice fiscale dell'utente da vaccinare.
	 */
	public void showDatiVaccinato(PrenotazioniVaccini prenotazioneVaccino, InfoCittadini infoCittadino) {
		
		label_InfoNomeCentro.setText(prenotazioneVaccino.getNomeCentro());
		label_NomeCittadino.setText(infoCittadino.getNomeCittadino());
		label_CognomeCittadino.setText(infoCittadino.getCognomeCittadino());
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		label_DataVaccino.setText(dateFormat.format(prenotazioneVaccino.getData()));
		
		tf_Cf.setEditable(false);
		obtainDatas.setEnabled(false);
		cb_TipoVaccino.setEnabled(true);
		tf_IdVaccinazione.setEnabled(true);
		
		panel_AddVaccinatoGUI.add(registraVaccinato);
		panel_AddVaccinatoGUI.validate();
		panel_AddVaccinatoGUI.repaint();
	}

	public void deleteView() {
		frame_AddVaccinatoGUI.setVisible(false);
		frame_AddVaccinatoGUI.dispose();
	}
}
