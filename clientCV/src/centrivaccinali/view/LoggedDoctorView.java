package centrivaccinali.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import centrivaccinali.controller.LoggedDoctorController;
import client.HomepageView;
import client.Utility;
import interfaccia.Server;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

/**
 * Classe che permette agli operatori sanitari di interfacciarsi con le possibili opzioni. 
 * 
 * @author Nico
 */
public class LoggedDoctorView extends JFrame {
	
	private static final int WIDTH 	= 800;
	private static final int HEIGHT = 600;
	
	private JFrame 	frame_LoggedDoctor;
	private JPanel 	panel_DoctorContainer;
	private JPanel 	panel_LoggedDoctorSideMenu;
	private JPanel 	panel_LoggedDoctorWindow;
	public JLabel 	id;
	private JButton button_CreateCentroVaccinale;
	private JButton button_AggiungiVaccinati;
	private LoggedDoctorController controller;
	
	public LoggedDoctorView() {
		controller = new LoggedDoctorController(this);
		
		frame_LoggedDoctor = new JFrame();
		panel_DoctorContainer = new JPanel();
		panel_LoggedDoctorSideMenu = new JPanel();
		panel_LoggedDoctorWindow = new JPanel();
		
		frame_LoggedDoctor.setTitle("CENTRI VACCINALI");
		frame_LoggedDoctor.setSize(WIDTH, HEIGHT);
		frame_LoggedDoctor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame_LoggedDoctor.setResizable(false);
		frame_LoggedDoctor.setLocationRelativeTo(null);
		frame_LoggedDoctor.add(panel_DoctorContainer);
		
		panel_DoctorContainer.setLayout(new BoxLayout(panel_DoctorContainer, BoxLayout.X_AXIS));
		
		panel_LoggedDoctorWindow.setPreferredSize(new Dimension(750, 600));
		panel_LoggedDoctorWindow.setLocation(50, 0);
		panel_LoggedDoctorWindow.setBackground(Color.WHITE);
		panel_LoggedDoctorWindow.setLayout(null);
		panel_LoggedDoctorSideMenu.setPreferredSize(new Dimension(50, 600));
		panel_LoggedDoctorSideMenu.setBounds(0, 0, 50, 600);
		panel_LoggedDoctorSideMenu.setBackground(Color.ORANGE);
		
		panel_DoctorContainer.add(panel_LoggedDoctorSideMenu);
		panel_DoctorContainer.add(panel_LoggedDoctorWindow);
		
		ImageIcon menu = new ImageIcon("ext-img//Menu.png");
		JLabel l_Show = new JLabel(menu);
		l_Show.setBounds(20, 10, 26, 26);
		panel_LoggedDoctorSideMenu.add(l_Show);
		
		ImageIcon account = new ImageIcon("ext-img//User.png");
		JLabel l_Account = new JLabel(account);
		l_Account.setHorizontalAlignment(JLabel.CENTER);
		l_Account.setVerticalAlignment(JLabel.CENTER);
		l_Account.setBounds(92, 50, 66, 66);
		
		id = new JLabel("");
		id.setBounds(25, 120, 200, 25);
		id.setHorizontalAlignment(JLabel.CENTER);
		id.setVerticalAlignment(JLabel.CENTER);
		
		JButton button_LogOut = new JButton("LOGOUT");
		button_LogOut.setBounds(75, 450, 100, 25);
		
		button_LogOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.logOutAction();
			}
		});
		
		l_Show.addMouseListener(new MouseAdapter() {
			boolean show = true;
			public void mouseReleased(MouseEvent e) {
				if(show) {
					button_CreateCentroVaccinale.setEnabled(false);
					button_AggiungiVaccinati.setEnabled(false);
					panel_LoggedDoctorWindow.setLocation(250, 0);
					panel_LoggedDoctorSideMenu.setSize(new Dimension(250, 600));
					l_Show.setBounds(213, 5, 26, 26);
					panel_LoggedDoctorSideMenu.add(l_Account);
					panel_LoggedDoctorSideMenu.add(id);
					panel_LoggedDoctorSideMenu.add(button_LogOut);
					show = false;
				} else {
					button_CreateCentroVaccinale.setEnabled(true);
					button_AggiungiVaccinati.setEnabled(true);
					panel_LoggedDoctorWindow.setLocation(50, 0);
					panel_LoggedDoctorSideMenu.setSize(new Dimension(50, 600));
					l_Show.setBounds(13, 5, 26, 26);
					panel_LoggedDoctorSideMenu.remove(l_Account);
					panel_LoggedDoctorSideMenu.remove(id);
					panel_LoggedDoctorSideMenu.remove(button_LogOut);
					panel_LoggedDoctorSideMenu.validate();
					panel_LoggedDoctorSideMenu.repaint();
					show = true;
				}
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				l_Show.setOpaque(true);
				l_Show.setBackground(Color.WHITE);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				l_Show.setOpaque(true);
				l_Show.setBackground(Color.ORANGE);
			}
			
		});
		
		button_CreateCentroVaccinale = new JButton("CREA CENTRO VACCINALE");
		button_CreateCentroVaccinale.setBounds(250, 50, 200, 50);
		panel_LoggedDoctorWindow.add(button_CreateCentroVaccinale);
		
		button_CreateCentroVaccinale.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.createCentroVaccinaleAction();
			}
		});
		
		button_AggiungiVaccinati = new JButton("INSERISCI VACCINATO");
		button_AggiungiVaccinati.setBounds(250, 200, 200, 50);
		panel_LoggedDoctorWindow.add(button_AggiungiVaccinati);
		
		button_AggiungiVaccinati.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.aggiungiVaccinatiAction();
			}
		});
		
		frame_LoggedDoctor.setVisible(true);
	}
	
	public void deleteView() {
		frame_LoggedDoctor.setVisible(false);
		frame_LoggedDoctor.dispose();
	}
}
