package centrivaccinali.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import centrivaccinali.controller.AddCentroVaccinaleController;
import client.Utility;
import comune.InfoCentriVaccinali;
import interfaccia.Server;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

/**
 * Classe che permette all'operatore sanitario di inserire un nuovo centro vaccinale.
 * 
 * @author Nico
 */
public class AddCentroVaccinaleView extends JFrame {
	
	private static final int WIDTH 		= 380;
	private static final int HEIGHT 	= 400;
	
	private JFrame 							frame_AddCentroVaccinale;
	private JPanel 							panel_AddCentroVaccinale;
	private JTextField 						tf_NomeCentro;
	private JTextField 						tf_Indirizzo;
	private JTextField 						tf_TipologiaCentro;
	private AddCentroVaccinaleController 	controller;
	
	public AddCentroVaccinaleView() {
		
		controller = new AddCentroVaccinaleController(this);
		
		frame_AddCentroVaccinale = new JFrame();
		panel_AddCentroVaccinale = new JPanel();
		frame_AddCentroVaccinale.setTitle("AGGIUNGI CENTRO VACCINALE");
		frame_AddCentroVaccinale.setSize(WIDTH, HEIGHT);
		frame_AddCentroVaccinale.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame_AddCentroVaccinale.setResizable(false);
		frame_AddCentroVaccinale.setLocationRelativeTo(null);
		frame_AddCentroVaccinale.add(panel_AddCentroVaccinale);
		
		panel_AddCentroVaccinale.setLayout(null);
		
		JLabel label_NomeCentro = new JLabel("Nome centro");
		label_NomeCentro.setBounds(10, 50, 100, 25);
		panel_AddCentroVaccinale.add(label_NomeCentro);
		
		tf_NomeCentro = new JTextField(1);
		tf_NomeCentro.setBounds(150, 50, 200, 25);
		panel_AddCentroVaccinale.add(tf_NomeCentro);
		
		JLabel label_Indirizzo = new JLabel("Indirizzo");
		label_Indirizzo.setBounds(10, 80, 100, 25);
		panel_AddCentroVaccinale.add(label_Indirizzo);
		
		tf_Indirizzo = new JTextField(1);
		tf_Indirizzo.setBounds(150, 80, 200, 25);
		panel_AddCentroVaccinale.add(tf_Indirizzo);
		
		JLabel label_TipologiaCentro = new JLabel("Tipo centro");
		label_TipologiaCentro.setBounds(10, 110, 100, 25);
		panel_AddCentroVaccinale.add(label_TipologiaCentro);
		
		tf_TipologiaCentro = new JTextField(1);
		tf_TipologiaCentro.setBounds(150, 110, 200, 25);
		panel_AddCentroVaccinale.add(tf_TipologiaCentro);
		
		JButton button_AddCentro = new JButton("REGISTRA CENTRO");
		button_AddCentro.setBounds(30, 200, 150, 25);
		panel_AddCentroVaccinale.add(button_AddCentro);
		
		button_AddCentro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nomeCentroVaccinale = tf_NomeCentro.getText();
				String indirizzoCentroVaccinale = tf_Indirizzo.getText();
				String tipologiaCentroVaccinale = tf_TipologiaCentro.getText();
				controller.registraCentroVaccinaleAction(nomeCentroVaccinale, indirizzoCentroVaccinale, tipologiaCentroVaccinale);
			}
		});
		
		/**
		 * Bottone che resetta i valori in tutti i campi
		 */
		JButton button_Reset = new JButton("RESET");
		button_Reset.setBounds(190, 200, 150, 25);
		panel_AddCentroVaccinale.add(button_Reset);
		
		button_Reset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.resetAction();
			}
		});
		
		frame_AddCentroVaccinale.setVisible(true);
	}
	
	/**
	 * Metodo che resetta i valori dei campi.
	 */
	public void reset() {
		tf_NomeCentro.setText("");
		tf_Indirizzo.setText("");
		tf_TipologiaCentro.setText("");
	}
	
	/**
	 * Metodo che chiude il frame.
	 */
	public void deleteView() {
		frame_AddCentroVaccinale.setVisible(false);
		frame_AddCentroVaccinale.dispose();
	}
}
