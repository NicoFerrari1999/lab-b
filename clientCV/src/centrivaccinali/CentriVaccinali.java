package centrivaccinali;

import client.ClientStart;
/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

/**
 * Classe principale per avviare il client
 * 
 * @author Nico
 */
public class CentriVaccinali {

	public static void main(String[] args) {
		
		ClientStart.start();
	
	}
}