package cittadini.controller;

import java.awt.event.KeyEvent;
import java.rmi.RemoteException;

import cittadini.view.AddEventoAvversoView;
import client.ServerSingleton;
import client.Utility;
import comune.EventiAvversi;
import interfaccia.Server;

public class AddEventoAvversoController {

	private AddEventoAvversoView view;
	
	private Server stub;
	
	public AddEventoAvversoController(AddEventoAvversoView view) {
		this.view = view;
		this.stub = ServerSingleton.getInstance();
	}
	
	public void checkNumCharAction(KeyEvent e, int countCharNote) {
		if(countCharNote < 1) {
			Utility.mostraErrorePopUp("ERRORE", "Raggiunto il limite massimo di caratteri inseribili");
			e.consume();
			return;
		}
	}
	
	public void inserisciEventiAvversiAction(Integer[] intensitaEventi, String[] noteEventi, String idVaccinazione, String nomeCentro, String[] eventi) {
		
		if(Integer.parseInt(idVaccinazione) == 0) {
			Utility.mostraErrorePopUp("ERRORE", "Impossibile registrare un evento avverso senza essersi prima vaccinati");
			return;
		}
		
		EventiAvversi eventoAvverso = new EventiAvversi(Integer.parseInt(idVaccinazione), nomeCentro, eventi, intensitaEventi, noteEventi);
		
		try {
			int risultato = stub.InserisciEventiAvversi(eventoAvverso);
			if(risultato == 2) {
				Utility.mostraErrorePopUp("ERRORE", "Non ti sei vaccinato in questo centro vaccinale");
				return;
			}
			if(risultato == 1) {
				Utility.mostraInformazionePopUp("CONGRATULAZIONI", "Eventi avversi inseriti con successo");
				view.deleteView();
			}
		} catch (RemoteException ex) {
			ex.printStackTrace();
		}
	}
}
