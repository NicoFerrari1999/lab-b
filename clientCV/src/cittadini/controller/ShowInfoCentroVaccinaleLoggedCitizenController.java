package cittadini.controller;

import java.rmi.RemoteException;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.jdatepicker.impl.JDatePickerImpl;

import cittadini.view.AddEventoAvversoView;
import cittadini.view.ShowInfoCentroVaccinaleLoggedCitizenView;
import client.ServerSingleton;
import client.Utility;
import comune.PrenotazioniVaccini;
import interfaccia.Server;

public class ShowInfoCentroVaccinaleLoggedCitizenController {
	
	ShowInfoCentroVaccinaleLoggedCitizenView view;
	
	Server stub;
	
	public ShowInfoCentroVaccinaleLoggedCitizenController(ShowInfoCentroVaccinaleLoggedCitizenView view) {
		this.view = view;
		this.stub = ServerSingleton.getInstance();
	}
	
	public void prenotaVaccinoAction(JDatePickerImpl datePicker, String nomeCentro, String codiceFiscale) {
		if(datePicker.getJFormattedTextField().getText().equals("")) {
			Utility.mostraErrorePopUp("ERRORE", "Inserisci una data valida");
			return;
		}
		
		Date dataPrenotazione = Date.valueOf(datePicker.getJFormattedTextField().getText());
		
		DateTimeFormatter formatoData = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDateTime giornoAttuale = LocalDateTime.now();
		Date dataOdierna = Date.valueOf(giornoAttuale.format(formatoData));
		
		if(dataOdierna.compareTo(dataPrenotazione) > 0) {
			Utility.mostraInformazionePopUp("ATTENZIONE", "La data selezionata non � pi� disponibile");
			return;
		}
		
		PrenotazioniVaccini prenotazione = new PrenotazioniVaccini(nomeCentro, codiceFiscale, dataPrenotazione);
		
		try {
			int risultato = stub.prenotaVaccinazione(prenotazione);
			if(risultato == 2) {
				Utility.mostraInformazionePopUp("ATTENZIONE", "Questo CF � gi� stato usato per una registrazione");
				view.deleteView();
				return;
			}
			if(risultato == 1) {
				Utility.mostraInformazionePopUp("CONGRATULAZIONI", "Prenotazione avvenuta con successo");
				view.deleteView();
			}
		} catch (RemoteException ex) {
			ex.printStackTrace();
		}
	}
	
	public void registraEventoAvverso(String nomeCentro, String codiceFiscale) {
		try {
			int risultato = stub.controlloPreRegistrazioneEventoAvverso(codiceFiscale);
			if(risultato == 1) {
				Utility.mostraErrorePopUp("ERRORE", "Sono gi� stati inseriti degli eventi avversi per questo Id");
				return;
			}
		} catch (RemoteException ex) {
			ex.printStackTrace();
		}
		
		AddEventoAvversoView addEventoAvverso = new AddEventoAvversoView();
		
		addEventoAvverso.label_InfoNomeCentro.setText(nomeCentro);
		try {
			addEventoAvverso.label_IdVaccinazione.setText(stub.ottieniIdVaccinazione(codiceFiscale));
			view.deleteView();
		} catch (RemoteException ex) {
			ex.printStackTrace();
		}
	}

}
