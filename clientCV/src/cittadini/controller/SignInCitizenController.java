package cittadini.controller;

import java.rmi.RemoteException;

import cittadini.view.SignInCitizenView;
import client.ServerSingleton;
import client.Utility;
import comune.CitizenData;
import interfaccia.Server;

public class SignInCitizenController {
	
	SignInCitizenView view;
	
	Server stub;
	
	public SignInCitizenController(SignInCitizenView view) {
		this.view = view;
		this.stub = ServerSingleton.getInstance();
	}
	
	public void resetAction() {
		view.reset();
	}
	
	public void saveCitizenAction(String nome, String cognome, String cf, String email, String username, char[] pass1, char[] pass2) {
		String pw = String.valueOf(pass1);
		String pw2 = String.valueOf(pass2);
		
		if(nome.equals("") || cognome.equals("") || cf.equals("") || email.equals("") || username.equals("") || pw.equals("")) {
            Utility.mostraErrorePopUp("ERRORE", "Compila tutti i campi.");
            return;
        }
            
        if(!email.contains("@") || email.lastIndexOf("@") == (email.length()-1)) {
            Utility.mostraErrorePopUp("ERRORE", "Inserisci una email valida");
            return;
        }
        
        if(cf.length() != 16) {
        	Utility.mostraErrorePopUp("ERRORE", "Il Cf deve essere lungo 16 caratteri");
        	return;
        }
        
        if(username.length() < 4) {
        	Utility.mostraErrorePopUp("ERRORE", "Il nome utente deve essere lungo almeno 4 caratteri");
        	return;
        }
        
        if(!pw.equals(pw2)) {
            Utility.mostraErrorePopUp("ERRORE", "Le due password non coincidono");
            return;
        }
        
        pw = Utility.encryptMD5(pw);
        CitizenData tempCitizen = new CitizenData(nome, cognome, cf, email, username, pw);
        
        try {
			int risultato = stub.registraCittadino(tempCitizen);
			if(risultato == 1) {
				Utility.mostraInformazionePopUp("CONGRATULAZIONI", "Registrazione avvenuta con successo");
				view.deleteView();
			}
			if(risultato == 2) {
				Utility.mostraErrorePopUp("ERRORE", "Il CF inserito � gi� stato utilizzato");
        		return;
			}
		} catch (RemoteException ex) {
			ex.printStackTrace();
		}
	}

}
