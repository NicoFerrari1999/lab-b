package cittadini.controller;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import cittadini.view.LoggedCitizenView;
import cittadini.view.ShowInfoCentroVaccinaleLoggedCitizenView;
import client.HomepageView;
import client.ServerSingleton;
import client.Utility;
import comune.InfoCentriVaccinali;
import interfaccia.Server;

public class LoggedCitizenController {

	private LoggedCitizenView view;
	
	private Server stub;
	
	public LoggedCitizenController(LoggedCitizenView view) {
		this.view = view;
		this.stub = ServerSingleton.getInstance();
	}
	
	public void logoutAction() {
		Utility.mostraInformazionePopUp("", "Logout avvenuto con successo, arrivederci");
		HomepageView homepage = new HomepageView();
		view.deleteView();
	}
	
	public void showInTableAction(String research, DefaultTableModel tableModel) {
		List<InfoCentriVaccinali> centriVaccinali = new ArrayList<>();
		if(research != null && research.trim().length() > 1)
			try {
				centriVaccinali = stub.cercaCentroVaccinale(research);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		
		tableModel.setRowCount(0);
		
		for(int i=0; i<centriVaccinali.size(); i++) {
			String nome = centriVaccinali.get(i).getNomeCentro();
			String indirizzo = centriVaccinali.get(i).getIndirizzo();
			String tipo = centriVaccinali.get(i).getTipoCentro();
			
			Object[] infoData = {nome, indirizzo, tipo};
			
			tableModel.addRow(infoData);
		}
	}
	
	public void visualizzaInfoCentriVaccinaliAction(int index, TableModel model, String user) {
		ShowInfoCentroVaccinaleLoggedCitizenView showInfoCentro = new ShowInfoCentroVaccinaleLoggedCitizenView();
		InfoCentriVaccinali info = new InfoCentriVaccinali(model.getValueAt(index, 0).toString(), model.getValueAt(index, 1).toString(), model.getValueAt(index, 2).toString());

		showInfoCentro.infoNomeCentro.setText(info.getNomeCentro());
		showInfoCentro.infoIndirizzo.setText(info.getIndirizzo());
		showInfoCentro.infoTipoCentro.setText(info.getTipoCentro());
		
		try {
			String cf = stub.ottieniCf(user);
			showInfoCentro.cf.setText(cf);
			
		} catch (RemoteException ex) {
			ex.printStackTrace();
		}
	}
	
}
