package cittadini.controller;

import java.rmi.RemoteException;

import cittadini.view.LoggedCitizenView;
import cittadini.view.LoginCitizenView;
import cittadini.view.SignInCitizenView;
import client.HomepageView;
import client.ServerSingleton;
import client.Utility;
import interfaccia.Server;

public class LoginCitizenController {

	LoginCitizenView view;
	
	Server stub;
	
	public LoginCitizenController(LoginCitizenView view) { 
		this.view = view;
		this.stub = ServerSingleton.getInstance();
	}
	
	public void checkCitizenCredentialAction(String username, char[] password) {
		String typedPassword = String.valueOf(password);
		typedPassword = Utility.encryptMD5(typedPassword);
		if(username.equals("") || typedPassword.equals("")) {
            Utility.mostraErrorePopUp("ERRORE", "Compila tutti i campi.");
            return;
        }
		
		try {
			int risultato = stub.loginCittadino(username, typedPassword);
			if(risultato == 1) {
				LoggedCitizenView loggedCitizen = new LoggedCitizenView();
				loggedCitizen.username.setText(username);
				view.deleteView();
			}
			if(risultato == 2) {
				Utility.mostraErrorePopUp("ERRORE", "Username inesistente, registrati");
				return;
			}
			if(risultato == 3) {
				Utility.mostraErrorePopUp("ERRORE", "Username o Password non corretti");
				return;
			}
		} catch (RemoteException ex) {
			ex.printStackTrace();
		}
	}

	public void backAction() {
		HomepageView Homepage = new HomepageView();
		view.deleteView();
	}
	
	public void signInAction() {
		SignInCitizenView SignInCitizen = new SignInCitizenView();
	}
}
