package cittadini.view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import cittadini.controller.ShowInfoCentroVaccinaleLoggedCitizenController;
import client.DateLabelFormatter;
import client.Utility;
import comune.PrenotazioniVaccini;
import interfaccia.Server;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

/**
 * Classe che mostra le informazioni del centro vaccinale selezionato dall'utente dopo il login.
 * 
 * @author Nico
 */
public class ShowInfoCentroVaccinaleLoggedCitizenView extends JDialog {
	
	private static final int WIDTH 	= 400;
	private static final int HEIGHT	= 500;
	
	private JFrame 			frame_ShowInfoCentroVaccinaleLoggedCitizen;
	private JPanel 			panel_ShowInfoCentroVaccinaleLoggedCitizen;
	public JLabel 			infoNomeCentro;
	public JLabel 			infoIndirizzo;
	public JLabel 			infoTipoCentro;
	private JDatePickerImpl datePicker;
	public JLabel 			cf;
	private ShowInfoCentroVaccinaleLoggedCitizenController controller;
	
	public ShowInfoCentroVaccinaleLoggedCitizenView() {
		this.controller = new ShowInfoCentroVaccinaleLoggedCitizenController(this);
		
		frame_ShowInfoCentroVaccinaleLoggedCitizen = new JFrame();
		panel_ShowInfoCentroVaccinaleLoggedCitizen = new JPanel();
		
		frame_ShowInfoCentroVaccinaleLoggedCitizen.setTitle("INFO CENTRO VACCINALE");
		frame_ShowInfoCentroVaccinaleLoggedCitizen.setSize(WIDTH, HEIGHT);
		frame_ShowInfoCentroVaccinaleLoggedCitizen.setResizable(false);
		frame_ShowInfoCentroVaccinaleLoggedCitizen.setLocationRelativeTo(null);
		frame_ShowInfoCentroVaccinaleLoggedCitizen.add(panel_ShowInfoCentroVaccinaleLoggedCitizen);
		
		panel_ShowInfoCentroVaccinaleLoggedCitizen.setLayout(null);
		panel_ShowInfoCentroVaccinaleLoggedCitizen.setBackground(Color.WHITE);
		
		JLabel titolo = new JLabel("INFO PRENOTAZIONE VACCINO");
		titolo.setForeground(Color.RED);
		titolo.setBounds(0, 15, 400, 25);
		titolo.setHorizontalAlignment(JLabel.CENTER);
		titolo.setVerticalAlignment(JLabel.CENTER);
		panel_ShowInfoCentroVaccinaleLoggedCitizen.add(titolo);
		
		JLabel nomeCentro = new JLabel("Nome centro: ");
		nomeCentro.setForeground(Color.RED);
		nomeCentro.setBounds(0, 50, 400, 25);
		nomeCentro.setHorizontalAlignment(JLabel.CENTER);
		nomeCentro.setVerticalAlignment(JLabel.CENTER);
		panel_ShowInfoCentroVaccinaleLoggedCitizen.add(nomeCentro);
		
		infoNomeCentro = new JLabel("");
		infoNomeCentro.setBounds(0, 80, 400, 25);
		infoNomeCentro.setHorizontalAlignment(JLabel.CENTER);
		infoNomeCentro.setVerticalAlignment(JLabel.CENTER);
		panel_ShowInfoCentroVaccinaleLoggedCitizen.add(infoNomeCentro);
		
		JLabel indirizzo = new JLabel("Indirizzo: ");
		indirizzo.setForeground(Color.RED);
		indirizzo.setBounds(0, 110, 400, 25);
		indirizzo.setHorizontalAlignment(JLabel.CENTER);
		indirizzo.setVerticalAlignment(JLabel.CENTER);
		panel_ShowInfoCentroVaccinaleLoggedCitizen.add(indirizzo);
		
		infoIndirizzo = new JLabel("");
		infoIndirizzo.setBounds(0, 140, 400, 25);
		infoIndirizzo.setHorizontalAlignment(JLabel.CENTER);
		infoIndirizzo.setVerticalAlignment(JLabel.CENTER);
		panel_ShowInfoCentroVaccinaleLoggedCitizen.add(infoIndirizzo);
		
		JLabel tipoCentro = new JLabel("Tipo centro:");
		tipoCentro.setForeground(Color.RED);
		tipoCentro.setBounds(0, 170, 400, 25);
		tipoCentro.setHorizontalAlignment(JLabel.CENTER);
		tipoCentro.setVerticalAlignment(JLabel.CENTER);
		panel_ShowInfoCentroVaccinaleLoggedCitizen.add(tipoCentro);
		
		infoTipoCentro = new JLabel("");
		infoTipoCentro.setBounds(0, 200, 400, 25);
		infoTipoCentro.setHorizontalAlignment(JLabel.CENTER);
		infoTipoCentro.setVerticalAlignment(JLabel.CENTER);
		panel_ShowInfoCentroVaccinaleLoggedCitizen.add(infoTipoCentro);
		
		JLabel codiceFiscale = new JLabel("Codice Fiscale: ");
		codiceFiscale.setForeground(Color.RED);
		codiceFiscale.setBounds(0, 230, 400, 25);
		codiceFiscale.setHorizontalAlignment(JLabel.CENTER);
		codiceFiscale.setVerticalAlignment(JLabel.CENTER);
		panel_ShowInfoCentroVaccinaleLoggedCitizen.add(codiceFiscale);
		
		cf = new JLabel("");
		cf.setBounds(0, 260, 400, 25);
		cf.setHorizontalAlignment(JLabel.CENTER);
		cf.setVerticalAlignment(JLabel.CENTER);
		panel_ShowInfoCentroVaccinaleLoggedCitizen.add(cf);
		
		JLabel dataRegistrazione = new JLabel("Data vaccino: ");
		dataRegistrazione.setForeground(Color.RED);
		dataRegistrazione.setBounds(0, 290, 400, 25);
		dataRegistrazione.setHorizontalAlignment(JLabel.CENTER);
		dataRegistrazione.setVerticalAlignment(JLabel.CENTER);
		panel_ShowInfoCentroVaccinaleLoggedCitizen.add(dataRegistrazione);
		
		UtilDateModel model = new UtilDateModel();
		Properties p = new Properties();
		p.put("text.oggi", "Oggi");
		p.put("text.mese", "Mese");
		p.put("text.anno", "Anno");
		JDatePanelImpl datePanel = new JDatePanelImpl(model, p);
		datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
		datePicker.setBounds(100, 320, 200, 25);
		datePicker.setForeground(Color.WHITE);
		panel_ShowInfoCentroVaccinaleLoggedCitizen.add(datePicker);
		
		JButton button_Registrazione = new JButton("PRENOTA VACCINO");
		button_Registrazione.setBounds(100, 360, 200, 25);
		panel_ShowInfoCentroVaccinaleLoggedCitizen.add(button_Registrazione);
		
		/**
		 * Metodo che, dopo vari controlli, si interfaccia col server permettendo di prenotarsi per una vaccinazione presso un centro vaccinale
		 */
		button_Registrazione.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nomeCentro = infoNomeCentro.getText();
				String codiceFiscale = cf.getText();
				controller.prenotaVaccinoAction(datePicker, nomeCentro, codiceFiscale);
			}
		});
		
		JButton button_addEventoAvverso = new JButton("INSERISCI EVENTO AVVERSO");
		button_addEventoAvverso.setBounds(90, 400, 220, 25);
		panel_ShowInfoCentroVaccinaleLoggedCitizen.add(button_addEventoAvverso);
		
		/**
		 * Metodo che invia i dati degli eventi avversi al server per registrarli.
		 */
		button_addEventoAvverso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nomeCentro = infoNomeCentro.getText();
				String codiceFiscale = cf.getText();
				controller.registraEventoAvverso(nomeCentro, codiceFiscale);
			}
		});
		
		frame_ShowInfoCentroVaccinaleLoggedCitizen.setVisible(true);
	}
	
	public void deleteView() {
		frame_ShowInfoCentroVaccinaleLoggedCitizen.setVisible(false);
		frame_ShowInfoCentroVaccinaleLoggedCitizen.dispose();
	}
}
