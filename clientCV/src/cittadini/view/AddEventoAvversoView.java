package cittadini.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.rmi.RemoteException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import cittadini.controller.AddEventoAvversoController;
import client.Utility;
import comune.EventiAvversi;
import interfaccia.Server;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

/**
 * Classe che permette al cittadino di inserire un report sugli eventi avversi
 * dopo che � stato vaccinato.
 * 
 * @author Nico
 */
public class AddEventoAvversoView extends JFrame {
	
	private static final int WIDTH 	= 900;
	private static final int HEIGHT	= 700;
	
	private JFrame frame_AddEventoAvverso;
	private JPanel panel_tipoEvento;
	private JPanel panel_intensitaEvento;
	private JPanel panel_noteEvento;
	
	public JLabel label_InfoNomeCentro;
	public JLabel label_IdVaccinazione;
	
	String[] eventi = {
			"Mal di testa",
			"Febbre",
			"Dolori muscolari e articolari",
			"Linfoadenopatia",
			"Tachicardia",
			"Crisi ipertensiva",
	};
	
	ButtonGroup[] buttonGroups = new ButtonGroup[6];
	
	JTextField[] noteFields = new JTextField[6];
	
	JScrollPane[] scrollPane = new JScrollPane[6];
	
	AddEventoAvversoController controller;
	
	public AddEventoAvversoView() {
		controller = new AddEventoAvversoController(this);
		
		frame_AddEventoAvverso = new JFrame();
		JPanel container = new JPanel();
		container.setLayout(new BorderLayout());
		
		JPanel panel_infoCentro = new JPanel();
		panel_tipoEvento = new JPanel();
		panel_intensitaEvento = new JPanel();
		panel_noteEvento = new JPanel();
		JPanel panel_Button = new JPanel();
		
		frame_AddEventoAvverso.setTitle("INSERIMENTO EVENTI AVVERSI");
		frame_AddEventoAvverso.setSize(WIDTH, HEIGHT);
		frame_AddEventoAvverso.setResizable(false);
		frame_AddEventoAvverso.setLocationRelativeTo(null);
		frame_AddEventoAvverso.add(container);
		
		panel_infoCentro.setLayout(null);
		panel_infoCentro.setPreferredSize(new Dimension(WIDTH, 200));
		panel_infoCentro.setBackground(Color.WHITE);
		
		panel_tipoEvento.setLayout(new GridLayout(6, 1, 0, 30));
		panel_tipoEvento.setPreferredSize(new Dimension(WIDTH/3, 400));
		panel_tipoEvento.setBackground(Color.WHITE);
		
		panel_intensitaEvento.setLayout(new GridLayout(6, 5, 0, 30));
		panel_intensitaEvento.setPreferredSize(new Dimension(WIDTH/3, 400));
		panel_intensitaEvento.setBackground(Color.WHITE);
		
		panel_noteEvento.setLayout(new GridLayout(6, 2, 0, 30));
		panel_noteEvento.setPreferredSize(new Dimension(WIDTH/3, 400));
		panel_noteEvento.setBackground(Color.WHITE);
		
		panel_Button.setLayout(null);
		panel_Button.setPreferredSize(new Dimension(WIDTH, 100));
		panel_Button.setBackground(Color.WHITE);
		
		container.add(panel_infoCentro, BorderLayout.PAGE_START);
		container.add(panel_tipoEvento, BorderLayout.LINE_START);
		container.add(panel_intensitaEvento, BorderLayout.CENTER);
		container.add(panel_noteEvento, BorderLayout.LINE_END);
		container.add(panel_Button, BorderLayout.PAGE_END);
		
		JLabel nomeCentro = new JLabel("Nome centro: ");
		nomeCentro.setForeground(Color.RED);
		nomeCentro.setBounds(0, 20, WIDTH, 25);
		nomeCentro.setHorizontalAlignment(JLabel.CENTER);
		nomeCentro.setVerticalAlignment(JLabel.CENTER);
		panel_infoCentro.add(nomeCentro);
		
		label_InfoNomeCentro = new JLabel("");
		label_InfoNomeCentro.setBounds(0, 50, WIDTH, 25);
		label_InfoNomeCentro.setHorizontalAlignment(JLabel.CENTER);
		label_InfoNomeCentro.setVerticalAlignment(JLabel.CENTER);
		panel_infoCentro.add(label_InfoNomeCentro);
		
		JLabel idVaccinazione = new JLabel("Id vaccinazione: ");
		idVaccinazione.setForeground(Color.RED);
		idVaccinazione.setBounds(0, 80, WIDTH, 25);
		idVaccinazione.setHorizontalAlignment(JLabel.CENTER);
		idVaccinazione.setVerticalAlignment(JLabel.CENTER);
		panel_infoCentro.add(idVaccinazione);
		
		label_IdVaccinazione = new JLabel("");
		label_IdVaccinazione.setBounds(0, 110, WIDTH, 25);
		label_IdVaccinazione.setHorizontalAlignment(JLabel.CENTER);
		label_IdVaccinazione.setVerticalAlignment(JLabel.CENTER);
		panel_infoCentro.add(label_IdVaccinazione);
		
		JLabel eventiAvversi = new JLabel("EVENTI AVVERSI: ");
		eventiAvversi.setForeground(Color.RED);
		eventiAvversi.setBounds(0, 140, WIDTH, 25);
		eventiAvversi.setHorizontalAlignment(JLabel.CENTER);
		eventiAvversi.setVerticalAlignment(JLabel.CENTER);
		panel_infoCentro.add(eventiAvversi);
		
		JLabel tipoEvento = new JLabel("Tipo evento: ");
		tipoEvento.setForeground(Color.RED);
		tipoEvento.setBounds(0, 170, WIDTH/3, 25);
		tipoEvento.setHorizontalAlignment(JLabel.CENTER);
		tipoEvento.setVerticalAlignment(JLabel.CENTER);
		panel_infoCentro.add(tipoEvento);
		
		addEventi();
		
		JLabel intensita = new JLabel("Intensit�: ");
		intensita.setForeground(Color.RED);
		intensita.setBounds(WIDTH/3, 170, WIDTH/3, 25);
		intensita.setHorizontalAlignment(JLabel.CENTER);
		intensita.setVerticalAlignment(JLabel.CENTER);
		panel_infoCentro.add(intensita);
		
		addIntensita();
		
		JLabel note = new JLabel("Note: ");
		note.setForeground(Color.RED);
		note.setBounds(WIDTH/3*2, 170, WIDTH/3, 25);
		note.setHorizontalAlignment(JLabel.CENTER);
		note.setVerticalAlignment(JLabel.CENTER);
		panel_infoCentro.add(note);
		
		addNote();
		
		JButton button_InserisciEventiAvversi = new JButton("INSERISCI EVENTI AVVERSI");
		button_InserisciEventiAvversi.setBounds(350, 20, 200, 25);
		panel_Button.add(button_InserisciEventiAvversi);
		
		/**
		 * Metodo che dopo aver eseguito vari controlli, invia al server i dati degli eventi avversi da memorizzare.
		 */
		button_InserisciEventiAvversi.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String idVaccinazione = label_IdVaccinazione.getText();
				String nomeCentro = label_InfoNomeCentro.getText();
				
				Integer[] intensitaEventi = new Integer[6];
				String[] noteEventi = new String[6];
				
				for(int i=0; i<6; i++) {
					intensitaEventi[i] = Integer.parseInt(buttonGroups[i].getSelection().getActionCommand());
					noteEventi[i] = noteFields[i].getText();
				}
				
				controller.inserisciEventiAvversiAction(intensitaEventi, noteEventi, idVaccinazione, nomeCentro, eventi);
			}
			
		});
		
		frame_AddEventoAvverso.setVisible(true);
	}
	
	/**
	 * Metodo che crea, per ogni evento una nuova label e la inserisce nel panel.
	 */
	private void addEventi() {
		
		for(String evt : eventi) {
			JLabel evento = new JLabel(evt);
			evento.setHorizontalAlignment(JLabel.CENTER);
			panel_tipoEvento.add(evento);
		}
	}
	
	/**
	 * Metodo che crea i gruppi di bottoni con determinati valori d'intensita' e gli aggiunge al panel. 
	 */
	private void addIntensita() {	
		for(int j=0; j<6; j++) {
			buttonGroups[j] = new ButtonGroup();
			for(int i=1; i<6; i++) {
				JRadioButton rb_Intensita = new JRadioButton(""+i, (i==1 ? true : false));
				rb_Intensita.setActionCommand(""+i);
				rb_Intensita.setVerticalTextPosition(JRadioButton.BOTTOM);
				rb_Intensita.setHorizontalTextPosition(JRadioButton.CENTER);
				buttonGroups[j].add(rb_Intensita);
				panel_intensitaEvento.add(rb_Intensita);
			}
		}
	}
	
	/**
	 * Metodo che crea un campo per l'inserimento delle note, e una label per indicare il numero di caratteri rimanenti, per poi
	 * aggiungerli al panel.
	 */
	private void addNote() {
		
		for(int j=0; j<6; j++) {
			scrollPane[j] = new JScrollPane();
			noteFields[j] = new JTextField();
			scrollPane[j].add(noteFields[j]);
			scrollPane[j].setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
			panel_noteEvento.add(noteFields[j]);
			JLabel label = new JLabel("256 caratteri rimanenti");
			panel_noteEvento.add(label);
			int index = j;
			noteFields[j].addKeyListener(new KeyListener() {
				private int maxCharNote = 256;
				private int countCharNote = 0;
				
				@Override
				public void keyTyped(KeyEvent e) {
					int notelength = noteFields[index].getText().length();
					countCharNote = maxCharNote - noteFields[index].getText().length();
					label.setText(countCharNote + " caratteri rimanenti");
					controller.checkNumCharAction(e, countCharNote);
					}

				@Override
				public void keyPressed(KeyEvent e) {}
				
				@Override
				public void keyReleased(KeyEvent e) {}
					
			});
		}
	}
	
	public void deleteView() {
		frame_AddEventoAvverso.setVisible(false);
		frame_AddEventoAvverso.dispose();
	}
}
