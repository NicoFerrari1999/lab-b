package cittadini.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import cittadini.controller.SignInCitizenController;
import client.Utility;
import comune.CitizenData;
import interfaccia.Server;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

/**
 * Classe che permette la registrazione dei cittadini. 
 * 
 * @author Luca
 */
public class SignInCitizenView extends JFrame {

	private static final int WIDTH 		= 380;
	private static final int HEIGHT 	= 400;
	
	private JFrame 				frame_SignInCitizen;
	private JPanel 				panel_SignInCitizen;
	private JTextField 			tf_Name;
	private JTextField 			tf_Surname;
	private JTextField 			tf_Cf;
	private JTextField 			tf_Email;
	private JTextField 			tf_Username;
	private JPasswordField 		pf_Password;
	private JPasswordField		pf_Password2;
	private SignInCitizenController controller;
	
	public SignInCitizenView() {
		this.controller = new SignInCitizenController(this);
		
		frame_SignInCitizen = new JFrame();
		panel_SignInCitizen = new JPanel();
		frame_SignInCitizen.setTitle("Registrazione cittadini");
		frame_SignInCitizen.setSize(WIDTH, HEIGHT);
		frame_SignInCitizen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame_SignInCitizen.setResizable(false);
		frame_SignInCitizen.setLocationRelativeTo(null);
		frame_SignInCitizen.add(panel_SignInCitizen);
		
		panel_SignInCitizen.setLayout(null);
		
		JLabel label_Name = new JLabel("Nome");
		label_Name.setBounds(10, 50, 60, 25);
		panel_SignInCitizen.add(label_Name);
		
		tf_Name = new JTextField(1);
		tf_Name.setBounds(150, 50, 200, 25);
		panel_SignInCitizen.add(tf_Name);
		
		JLabel label_Surname = new JLabel("Cognome");
		label_Surname.setBounds(10, 80, 60, 25);
		panel_SignInCitizen.add(label_Surname);
		
		tf_Surname = new JTextField(1);
		tf_Surname.setBounds(150, 80, 200, 25);
		panel_SignInCitizen.add(tf_Surname);
		
		JLabel label_Cf = new JLabel("CF");
		label_Cf.setBounds(10, 110, 60, 25);
		panel_SignInCitizen.add(label_Cf);
		
		tf_Cf = new JTextField(1);
		tf_Cf.setBounds(150, 110, 200, 25);
		panel_SignInCitizen.add(tf_Cf);
		
		JLabel label_Email = new JLabel("Email");
		label_Email.setBounds(10, 140, 60, 25);
		panel_SignInCitizen.add(label_Email);
		
		tf_Email = new JTextField(1);
		tf_Email.setBounds(150, 140, 200, 25);
		panel_SignInCitizen.add(tf_Email);
		
		JLabel label_Username = new JLabel("Username");
		label_Username.setBounds(10, 170, 60, 25);
		panel_SignInCitizen.add(label_Username);
		
		tf_Username = new JTextField(1);
		tf_Username.setBounds(150, 170, 200, 25);
		panel_SignInCitizen.add(tf_Username);
		
		JLabel label_Password = new JLabel("Password");
		label_Password.setBounds(10, 200, 60, 25);
		panel_SignInCitizen.add(label_Password);
		
		pf_Password = new JPasswordField();
		pf_Password.setBounds(150, 200, 200, 25);
		panel_SignInCitizen.add(pf_Password);
		
		JLabel secondPassword = new  JLabel("Conferma password");
		secondPassword.setBounds(10, 230, 120, 25);
		panel_SignInCitizen.add(secondPassword);
		
		pf_Password2 = new JPasswordField();
		pf_Password2.setBounds(150, 230, 200, 25);
		panel_SignInCitizen.add(pf_Password2);
		
		JButton button_SignIn = new JButton("SIGN-IN");
		button_SignIn.setBounds(50, 300, 80, 30);
		panel_SignInCitizen.add(button_SignIn);
		
		/**
		 * Metodo che, dopo aver effettuato vari controlli invia al server i dati del cittadino che vuole registrarsi.
		 */
		button_SignIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nome = tf_Name.getText();
				String cognome = tf_Surname.getText();
				String cf = tf_Cf.getText().toUpperCase();
				String email = tf_Email.getText();
				String username = tf_Username.getText();
				char[] password1 = pf_Password.getPassword();
				char[] password2 = pf_Password2.getPassword();
				controller.saveCitizenAction(nome, cognome, cf, email, username, password1, password2);
			}

		});
		
		JButton button_Reset = new JButton("RESET");
		button_Reset.setBounds(200, 300, 75, 30);
		panel_SignInCitizen.add(button_Reset);
		
		button_Reset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.resetAction();
			}
		});
		
		frame_SignInCitizen.setVisible(true);
	}
	
	/**
	 * Resetta i rispettivi campi.
	 */
	public void reset() {
		tf_Name.setText("");
		tf_Surname.setText("");
		tf_Cf.setText("");
		tf_Email.setText("");
		tf_Username.setText("");
		pf_Password.setText("");
		pf_Password2.setText("");
	}
	
	public void deleteView() {
		frame_SignInCitizen.setVisible(false);
		frame_SignInCitizen.dispose();
	}
}
