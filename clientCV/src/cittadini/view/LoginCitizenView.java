package cittadini.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;
import java.rmi.RemoteException;

import javax.swing.*;

import cittadini.controller.LoginCitizenController;
import client.HomepageView;
import client.Utility;
import interfaccia.Server;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

/**
 * Classe che permette il login dei cittadini.
 * 
 * @author Nico
 */
public class LoginCitizenView extends JFrame {
	
	private static final int WIDTH 	= 300;
	private static final int HEIGHT	= 300;
	
	private JFrame frame_LoginCitizen;
	private JPanel panel_LoginCitizen;
	private JTextField tf_Username;
	private JPasswordField pf_Password;
	private LoginCitizenController controller;
	
	public LoginCitizenView() {
		this.controller = new LoginCitizenController(this);
		
		frame_LoginCitizen = new JFrame();
		panel_LoginCitizen = new JPanel();
		frame_LoginCitizen.setTitle("CENTRI VACCINALI - LOGIN CITTADINO");
		frame_LoginCitizen.setSize(WIDTH, HEIGHT);
		frame_LoginCitizen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame_LoginCitizen.setResizable(false);
		frame_LoginCitizen.setLocationRelativeTo(null);
		frame_LoginCitizen.add(panel_LoginCitizen);
		
		panel_LoginCitizen.setLayout(null);
		
		JLabel back = new JLabel("BACK");
		back.setBounds(10, 10, 40, 25);
		panel_LoginCitizen.add(back);
		
		JLabel username = new JLabel("Username");
		username.setBounds(10, 50, 60, 25);
		panel_LoginCitizen.add(username);
		
		tf_Username = new JTextField(1);
		tf_Username.setBounds(85, 50, 180, 25);
		panel_LoginCitizen.add(tf_Username);
		
		JLabel password = new JLabel("Password");
		password.setBounds(10, 100, 60, 25);
		panel_LoginCitizen.add(password);
		
		pf_Password = new JPasswordField();
		pf_Password.setBounds(85, 100, 180, 25);
		panel_LoginCitizen.add(pf_Password);
		
		JLabel signIn = new JLabel("Nuovo utente? Registrati ora");
		signIn.setFont(new Font("Verdana", Font.ITALIC, 10));
		signIn.setBounds(100, 130, 160, 15);
		panel_LoginCitizen.add(signIn);
		
		JButton button_Login = new JButton("LOGIN");
		button_Login.setBounds(105, 160, 90, 20);
		panel_LoginCitizen.add(button_Login);
		
		/**
		 * Metodo che, dopo aver effettuato i vari controlli invia i dati al server, permettendo il login dei cittadini.
		 */
		button_Login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String username = tf_Username.getText();
				char[] password = pf_Password.getPassword();
				controller.checkCitizenCredentialAction(username, password);
			}
		});
		
		frame_LoginCitizen.setVisible(true);
		
		back.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseReleased(MouseEvent e) {
				controller.backAction();
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				back.setForeground(Color.RED);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				back.setForeground(Color.BLACK);
			}
			
			
		});
	
		signIn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				controller.signInAction();
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				signIn.setForeground(Color.BLUE);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				signIn.setForeground(Color.BLACK);
			}
			
		});
	}
	
	public void deleteView() {
		frame_LoginCitizen.setVisible(false);
		frame_LoginCitizen.dispose();
	}
}
