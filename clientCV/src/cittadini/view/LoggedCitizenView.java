package cittadini.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import cittadini.controller.LoggedCitizenController;
import client.HomepageView;
import client.Utility;
import comune.InfoCentriVaccinali;
import interfaccia.Server;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

/**
 * Classe che permette ai cittadini registrati di interfacciarsi con le opzioni possibili
 * 
 * @author Nico
 */
public class LoggedCitizenView extends JFrame {
	
	private static final int WIDTH 		= 800;
	private static final int HEIGHT		= 600;
	private static final String FIND	= "Ricerca per Nome, Comune, Provincia o Vaccino";
	
	private JFrame 				frame_LoggedCitizen;
	private JPanel 				panel_CitizenContainer;
	private JPanel 				panel_LoggedCitizenSideMenu;
	private JPanel 				panel_LoggedCitizenWindow;
	private JTable 				table;
	private JTextField 			find;
	private JButton 			search;
	public JLabel 				username;
	private LoggedCitizenController controller;
	
	public LoggedCitizenView() {
		controller = new LoggedCitizenController(this);
	
		frame_LoggedCitizen = new JFrame();
		panel_CitizenContainer = new JPanel();
		panel_LoggedCitizenSideMenu = new JPanel();
		panel_LoggedCitizenWindow = new JPanel();
		
		frame_LoggedCitizen.setTitle("CENTRI VACCINALI");
		frame_LoggedCitizen.setSize(WIDTH, HEIGHT);
		frame_LoggedCitizen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame_LoggedCitizen.setResizable(false);
		frame_LoggedCitizen.setLocationRelativeTo(null);
		frame_LoggedCitizen.add(panel_CitizenContainer);
		
		panel_CitizenContainer.setLayout(new BoxLayout(panel_CitizenContainer, BoxLayout.X_AXIS));
		
		panel_LoggedCitizenWindow.setPreferredSize(new Dimension(750, 600));
		panel_LoggedCitizenWindow.setLocation(50,  0);
		panel_LoggedCitizenWindow.setBackground(Color.WHITE);
		panel_LoggedCitizenWindow.setLayout(null);
		panel_LoggedCitizenSideMenu.setPreferredSize(new Dimension(50, 600));
		panel_LoggedCitizenSideMenu.setBounds(0, 0, 50, 600);
		panel_LoggedCitizenSideMenu.setBackground(Color.ORANGE);
		
		panel_CitizenContainer.add(panel_LoggedCitizenSideMenu);
		panel_CitizenContainer.add(panel_LoggedCitizenWindow);
		
		ImageIcon menu = new ImageIcon("ext-img//Menu.png");
		JLabel l_Show = new JLabel(menu);
		l_Show.setBounds(20, 10, 26, 26);
		panel_LoggedCitizenSideMenu.add(l_Show);
		
		ImageIcon account = new ImageIcon("ext-img//User.png");
		JLabel l_Account = new JLabel(account);
		l_Account.setHorizontalAlignment(JLabel.CENTER);
		l_Account.setVerticalAlignment(JLabel.CENTER);
		l_Account.setBounds(92, 50, 66, 66);
		
		username = new JLabel("");
		username.setBounds(25, 120, 200, 25);
		username.setHorizontalAlignment(JLabel.CENTER);
		username.setVerticalAlignment(JLabel.CENTER);
		
		JButton button_LogOut = new JButton("LOGOUT");
		button_LogOut.setBounds(75, 450, 100, 25);
		
		button_LogOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.logoutAction();
			}
		});
		
		l_Show.addMouseListener(new MouseAdapter() {
			boolean show = true;
			/**
			 * Gestisce lo slide del menu'.
			 */
			public void mouseReleased(MouseEvent e) {
				if(show) {
					find.setEnabled(false);
					search.setEnabled(false);
					panel_LoggedCitizenWindow.setLocation(250, 0);
					panel_LoggedCitizenSideMenu.setSize(new Dimension(250, 600));
					l_Show.setBounds(213, 5, 26, 26);
					panel_LoggedCitizenSideMenu.add(l_Account);
					panel_LoggedCitizenSideMenu.add(username);
					panel_LoggedCitizenSideMenu.add(button_LogOut);
					show = false;
				} else {
					find.setEnabled(true);
					search.setEnabled(true);
					table.setVisible(true);
					panel_LoggedCitizenWindow.setLocation(50, 0);
					panel_LoggedCitizenSideMenu.setSize(new Dimension(50, 600));
					l_Show.setBounds(13, 5, 26, 26);
					panel_LoggedCitizenSideMenu.remove(l_Account);
					panel_LoggedCitizenSideMenu.remove(username);
					panel_LoggedCitizenSideMenu.remove(button_LogOut);
					panel_LoggedCitizenSideMenu.validate();
					panel_LoggedCitizenSideMenu.repaint();
					show = true;
				}
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				l_Show.setOpaque(true);
				l_Show.setBackground(Color.WHITE);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				l_Show.setOpaque(true);
				l_Show.setBackground(Color.ORANGE);
			}
			
		});
		
		DefaultTableModel tableModel = new DefaultTableModel(new String[]{"Nome centro", "Indirizzo", "Tipo Centro"}, 0);
		table = new JTable(tableModel) {
			public boolean editCellAt(int row, int column, java.util.EventObject e) {
	            return false;
			}
		};
		table.getTableHeader().setReorderingAllowed(false);

		find = new JTextField(20);
		find.setText(FIND);
		find.setBounds(175, 300, 400, 29);
		panel_LoggedCitizenWindow.add(find);
		
		ImageIcon searchPicture = new ImageIcon("ext-img/Search.png");
		search = new JButton(searchPicture);
		search.setBounds(146, 300, 29, 29);
		search.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String researchText = find.getText();
				controller.showInTableAction(researchText, tableModel);
			}
		});
		
		table.setRowSelectionAllowed(false);
		JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setBounds(0, 330, 750, 270);
        panel_LoggedCitizenWindow.add(scrollPane);
        panel_LoggedCitizenWindow.add(search);
		
        /**
    	 * Metodo che mostra una nuova GUI contenente tutte le informazioni del centro vaccinale scelto.
    	 */
        table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int index = table.getSelectedRow();
				TableModel model = table.getModel();
				String user = username.getText();
				controller.visualizzaInfoCentriVaccinaliAction(index, tableModel, user);
			}
		});
        
		frame_LoggedCitizen.setVisible(true);
	}
	
	public void deleteView() {
		frame_LoggedCitizen.setVisible(false);
		frame_LoggedCitizen.dispose();
	}
}
