package server.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import comune.DoctorData;
import server.dao.GeneralDao;
import server.dao.interfacce.OperatoriSanitariDao;

public class OperatoriSanitariDaoImpl extends GeneralDao implements OperatoriSanitariDao {

	/**
	 * Metodo che inserisce i dati nella tabella operatori_sanitari.
	 * @param doctorData	I dati dell'operatore sanitario che si sta registrando.
	 */
	@Override
	public void insertOperatoreSanitazio(DoctorData doctorData) {
		String qAddOperatori_Sanitari = "INSERT INTO operatori_sanitari VALUES (?, ?, ?, ?, ?)";
		PreparedStatement pstmt;
		Connection connection = null;
		
		try {
			connection = openConnection();
			pstmt = connection.prepareStatement(qAddOperatori_Sanitari);
			pstmt.setString(1, doctorData.getName());
			pstmt.setString(2, doctorData.getSurname());
			pstmt.setString(3, doctorData.getDoctorId());
			pstmt.setString(4, doctorData.getEmail());
			pstmt.setString(5, doctorData.getPassword());
			pstmt.executeUpdate();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Metodo che controlla se la password inserita corrisponde a quella presente sul Db per quel determinato operatore sanitario.
	 * @param id		L'id dell'operatore sanitario registrato.
	 * @param password	La password dell'operatore sanitario.
	 * @return			Se la password dell'operatore sanitario e' corretta.
	 */
	@Override
	public boolean checkPwDoctor(String id, String password) {
		String qDoctorPasswordMatch = "SELECT id, password FROM operatori_sanitari WHERE id = ? AND password = ?";
		PreparedStatement pstmt;
		ResultSet rs;
		Connection connection = null;
		
		try {
			connection = openConnection();
			pstmt = connection.prepareStatement(qDoctorPasswordMatch);
			pstmt.setString(1, id);
			pstmt.setString(2, password);
			rs = pstmt.executeQuery();
			
			while(rs.next())
				return true;
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			closeConnection(connection);
		}
		return false;
	}

	/**
	 * Metodo che controlla se l'id inserito esiste nel Db.
	 * @param id 	L'id dell'operatore sanitario che sta tentando di registrarsi.
	 * @return		Se e' gia' stato utilizzato quel determinato id.
	 */
	@Override
	public boolean existDoctor(String id) {
		String qExistDoctorOnDb = "SELECT id FROM operatori_sanitari WHERE id = ?";
		PreparedStatement pstmt;
		ResultSet rs;
		Connection connection = null;
		
		try {
			connection = openConnection();
			pstmt = connection.prepareStatement(qExistDoctorOnDb);
			pstmt.setString(1, id);
			rs = pstmt.executeQuery();
			
			while(rs.next()) {
				return true;
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			closeConnection(connection);
		}
		return false;
	}

}
