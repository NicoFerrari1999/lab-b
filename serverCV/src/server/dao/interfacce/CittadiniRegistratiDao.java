package server.dao.interfacce;

import comune.CitizenData;

public interface CittadiniRegistratiDao {
	
	public void insertCittadino(CitizenData citizenData);
	public CitizenData getDatiCittadino(String cf);
	public boolean checkPwCittadino(String username, String password);
	public boolean existCittadino(String username);
	public CitizenData getCfCittadino(String username);
	public boolean existCfCittadino(String cf);
	public void updateIdCittadino(int id, String cf);
	public boolean existIdCittadino(int id);
	public int getIdCittadino(String cf);
	public int countCittadiniVaccinati();
}
