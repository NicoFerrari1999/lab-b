package server.dao.interfacce;

import comune.PrenotazioniVaccini;

public interface RegistrazioniVaccinazioniDao {
	
	public void insertPrenotazioneVaccinazione(PrenotazioniVaccini prenotazioneVaccino);
	public boolean existCf(String cf);
	public PrenotazioniVaccini getInfoPrenotazione(String cf);
	
}
