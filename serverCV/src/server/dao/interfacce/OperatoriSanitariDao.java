package server.dao.interfacce;

import comune.DoctorData;

public interface OperatoriSanitariDao {
	
	public void insertOperatoreSanitazio(DoctorData doctorData);
	public boolean checkPwDoctor(String id, String password);
	public boolean existDoctor(String id);
}
