package server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import comune.CitizenData;
import comune.DoctorData;
import comune.EventiAvversi;
import comune.InfoCentriVaccinali;
import comune.InfoCittadini;
import comune.PrenotazioniVaccini;
import comune.RegistrazioniVaccinati;
import interfaccia.Client;
import interfaccia.Server;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

public class ServerImpl extends UnicastRemoteObject implements Server {
	GestioneClient gestClient = new GestioneClient();

	protected ServerImpl() throws RemoteException {
		super();
	}
	
	@Override
	public Integer subscribeToEvents(Client client) throws RemoteException {
		return gestClient.subscribeToEvents(client);
	}
	
	@Override
	public void unsubscribeToEvents(Integer clientId) throws RemoteException {
		gestClient.unsubscribeToEvents(clientId);
	}
	
	//ok
	@Override
	public int registraCentroVaccinale(InfoCentriVaccinali infoCentroVaccinale) throws RemoteException {
		return gestClient.gestRegistraCentroVaccinale(infoCentroVaccinale);
	}
	
	//ok
	@Override
	public int registraCittadino(CitizenData citizenData) throws RemoteException {
		return gestClient.gestRegistraCittadino(citizenData);
	}
	//ok
	@Override
	public int loginCittadino(String username, String password) throws RemoteException {
		return gestClient.gestLoginCittadino(username, password);
	}
	
	//ok
	@Override
	public int registraDottore(DoctorData doctorData) throws RemoteException {
		return gestClient.gestRegistraDottore(doctorData);
	}
	
	//ok
	@Override
	public int loginDottore(String id, String password) throws RemoteException {
		return gestClient.gestLoginDottore(id, password);
	}
	
	@Override
	public List<InfoCentriVaccinali> cercaCentroVaccinale(String testo) throws RemoteException {
		return gestClient.gestRicercaCentroVaccinale(testo);
	}
	
	//ok
	@Override
	public String ottieniCf(String username) throws RemoteException {
		return gestClient.gestOttieniCF(username);
	}

	//ok
	@Override
	public int prenotaVaccinazione(PrenotazioniVaccini prenotazioneVaccino) throws RemoteException {
		return gestClient.gestPrenotazioneVaccino(prenotazioneVaccino);
	}

	//ok
	@Override
	public PrenotazioniVaccini getPrenotazioneVaccinazione(String cf) throws RemoteException {
		return gestClient.gestOttieniInfoPrenotazioneVaccino(cf);
	}
	
	//ok
	@Override
	public InfoCittadini getInfoCittadini(String cf) throws RemoteException {
		return gestClient.gestOttieniInfoCittadino(cf);
	}

	//ok
	@Override
	public int registraVaccinato(RegistrazioniVaccinati datiRegistrazione) throws RemoteException {
		return gestClient.gestRegistraVaccinato(datiRegistrazione);
	}
	
	//ok
	public int controlloPreRegistrazioneEventoAvverso(String cf) throws RemoteException {
		return gestClient.gestControlloPreRegistrazioneEventoAvverso(cf);
	}
	
	public boolean existPrenotazioneOnDb(String cf) throws RemoteException {
		return gestClient.gestExistCfRegistrazioneVaccinazione(cf);
	}
	
	//ok
	public String ottieniIdVaccinazione(String cf) throws RemoteException {
		return gestClient.gestOttieniIdVaccinazione(cf);
	}
	
	//ok
	@Override
	public int InserisciEventiAvversi(EventiAvversi eventoAvverso) throws RemoteException {
		return gestClient.gestInserimentoEventoAvverso(eventoAvverso);
	}
	
	//ok
	public int ottieniNumSegnalazioni(String nomeCentro) throws RemoteException {
		return gestClient.gestOttieniNumSegnalazioni(nomeCentro);
	}

	@Override
	public Double getImportanzaEvento(String nomeCentro, String evento) throws RemoteException {
		return gestClient.gestOttieniImportanzaEventi(nomeCentro, evento);
	}

	@Override
	public int[] getStatisticheHomepage() throws RemoteException {
		return gestClient.getStatistiche();
	}
}
