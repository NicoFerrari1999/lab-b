package server;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

public class Utility {
	
	/**
	 * Se la stringa passata come argomento contiene degli spazi li rimuove, creando una parola unica
	 * @param nome
	 * @return
	 */
	public static String getNameForQuery(String nome) {
		nome = nome.replaceAll("\\s", "");
		return nome;
	}

}
