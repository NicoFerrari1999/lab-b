package server;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import interfaccia.Server;
import server.dao.*;

/**
 * Nicol� Ferrari, Mat. 732707, Varese
 * Alessandro Formenti, Mat. 734465, Varese
 */

public class ServerStart {
	public static void main(String[] args) {
		if (args == null || args.length != 3) {
			System.out.println(
					"ERRORE. Per avviare il server, inserire i dati del DB come parametro (<URL> <username> <password>)");
			System.exit(0);
		}

		DaoFactory.setDatabaseType("PostgreSQL");
		GeneralDao.setDatabaseParams(args[0], args[1], args[2]);
		
		try {
			Server stub = (Server) new ServerImpl();
			Registry registry = LocateRegistry.createRegistry(1099);
			registry.rebind("serverCV", stub);
			System.out.println("Server avviato");
		} catch (Exception ex) {
			System.out.println(ex);
		}
}
}
