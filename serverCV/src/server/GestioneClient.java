package server;

import java.rmi.RemoteException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import comune.*;
import interfaccia.Client;
import server.dao.*;
import server.dao.interfacce.*;

public class GestioneClient {
	
	CittadiniRegistratiDao cittadiniRegistratiDao = (CittadiniRegistratiDao) DaoFactory.getDao("CittadiniRegistratiDao");
	OperatoriSanitariDao operatoriSanitariDao = (OperatoriSanitariDao) DaoFactory.getDao("OperatoriSanitariDao");
	CentriVaccinaliDao centriVaccinaliDao = (CentriVaccinaliDao) DaoFactory.getDao("CentriVaccinaliDao");
	EventiAvversiDao eventiAvversiDao = (EventiAvversiDao) DaoFactory.getDao("EventiAvversiDao");
	RegistrazioniVaccinazioniDao registrazioniVaccinazioniDao = (RegistrazioniVaccinazioniDao) DaoFactory.getDao("RegistrazioniVaccinazioniDao");
	ConcurrentHashMap<Integer, Client> clients = new ConcurrentHashMap<>();
	Integer clientCount = 0;
	
	public Integer subscribeToEvents(Client client) {
		clients.put(clientCount++, client);
		return clientCount;
	}
	
	public void updateAllClients(int nuoviCentri, int nuoviVaccinati) {
		Set<Integer> set = clients.keySet();
		List<Integer> deadClients = new LinkedList<>();
		for(Integer key : set) {
			try {
				clients.get(key).update(new int[] {nuoviCentri, nuoviVaccinati});
			} catch (RemoteException ex) {
				deadClients.add(key);
			};
		}
		
		for(Integer key : deadClients) {
			unsubscribeToEvents(key);
		}
	}
	
	public void unsubscribeToEvents(Integer key) {
		clients.remove(key);
	}
	
	public int gestRegistraCittadino(CitizenData citizenData) {
		if(cittadiniRegistratiDao.existCfCittadino(citizenData.getCf()))
			return 2;
		else
			if(!cittadiniRegistratiDao.existCittadino(citizenData.getUsername())) {
				cittadiniRegistratiDao.insertCittadino(citizenData);
				return 1;
			}
		return 0;
	}
	
	public int gestLoginCittadino(String username, String pw) {
		if(!cittadiniRegistratiDao.existCittadino(username))
			return 2;
		else if(cittadiniRegistratiDao.checkPwCittadino(username, pw))//se password inserita = pw sul db -> login)
			return 1;
		else
			return 3;
	}
	
	public int gestRegistraDottore(DoctorData doctorData) {
		if(operatoriSanitariDao.existDoctor(doctorData.getDoctorId()))
			return 2;
		else {
			operatoriSanitariDao.insertOperatoreSanitazio(doctorData);;
			return 1;
		}
	}
	
	public int gestLoginDottore(String id, String pw) {
		if(!operatoriSanitariDao.existDoctor(id))
			return 2;
		else if(operatoriSanitariDao.checkPwDoctor(id, pw))
			return 1;
		else 
			return 3;
	}
	
	public String gestOttieniCF(String username) {
		CitizenData datiCittadino = cittadiniRegistratiDao.getCfCittadino(username);
		return datiCittadino.getCf();
	}
	
	public PrenotazioniVaccini gestOttieniInfoPrenotazioneVaccino(String cf) {
		return registrazioniVaccinazioniDao.getInfoPrenotazione(cf);
	}
	
	public InfoCittadini gestOttieniInfoCittadino(String cf) {
		CitizenData datiCittadino = cittadiniRegistratiDao.getDatiCittadino(cf);
		return new InfoCittadini(datiCittadino.getName(), datiCittadino.getSurname());
	}
	
	public int gestRegistraVaccinato(RegistrazioniVaccinati datoRegistrazione) {
		if(centriVaccinaliDao.existCf(datoRegistrazione.getNomeCentro(), datoRegistrazione.getCf()))
			return 3;
		else 
			if(cittadiniRegistratiDao.existIdCittadino(datoRegistrazione.getIdVaccinazione()))
				return 2;
			else {
				centriVaccinaliDao.insertVaccinato(datoRegistrazione);
				cittadiniRegistratiDao.updateIdCittadino(datoRegistrazione.getIdVaccinazione(), datoRegistrazione.getCf());;
				updateAllClients(0, 1);
				return 1;
			}
	}

	public int gestRegistraCentroVaccinale(InfoCentriVaccinali infoCentroVaccinale) {
		if(centriVaccinaliDao.existCentroVaccinale(infoCentroVaccinale.getNomeCentro()))
			return 2;
		else {
			centriVaccinaliDao.insertDatiCentroVaccinale(infoCentroVaccinale);
			String nome = infoCentroVaccinale.getNomeCentro();
			nome = Utility.getNameForQuery(nome);
			centriVaccinaliDao.createVaccinati_(nome);
			updateAllClients(1, 0);
			return 1;
		}
	}
	
	public int gestPrenotazioneVaccino(PrenotazioniVaccini prenotazioneVaccino) {
		if(registrazioniVaccinazioniDao.existCf(prenotazioneVaccino.getCf()))
			return 2;
		else
			registrazioniVaccinazioniDao.insertPrenotazioneVaccinazione(prenotazioneVaccino);
		return 1;
	}
	
	public boolean gestExistCfRegistrazioneVaccinazione(String cf) {
		return registrazioniVaccinazioniDao.existCf(cf);
	}

	public int gestControlloPreRegistrazioneEventoAvverso(String cf) {
		if(eventiAvversiDao.existId(cittadiniRegistratiDao.getIdCittadino(cf)))
			return 1;
		return 2;
	}
	
	public String gestOttieniIdVaccinazione(String cf) {
		return Integer.toString(cittadiniRegistratiDao.getIdCittadino(cf));
	}

	public List<InfoCentriVaccinali>gestRicercaCentroVaccinale(String testo) {
		return centriVaccinaliDao.findCentroVaccinale(testo);
	}

	public int gestInserimentoEventoAvverso(EventiAvversi eventoAvverso) {
		if(!centriVaccinaliDao.existId(eventoAvverso.getNomeCentro(), eventoAvverso.getIdVaccinazione()))
			return 2;
		else {
			for(int i=0; i<6; i++) {
				eventiAvversiDao.insertEventoAvverso(eventoAvverso.getIdVaccinazione(), 
						eventoAvverso.getNomeCentro(), 
						eventoAvverso.getEvento()[i], 
						eventoAvverso.getSeverita()[i], 
						eventoAvverso.getNotes()[i]);
			}
			return 1;
		}
	}
	
	public int gestOttieniNumSegnalazioni(String nomeCentro) {
		return eventiAvversiDao.getSegnalazioni(nomeCentro)/6;
	}
	
	public Double gestOttieniImportanzaEventi(String nomeCentro, String evento) {
		return eventiAvversiDao.getImportanzaEvento(nomeCentro, evento);
	}

	public int[] getStatistiche() {
		int countCentri = centriVaccinaliDao.countCentriVaccinali();
		int countVaccinati = cittadiniRegistratiDao.countCittadiniVaccinati();
		int[] statistiche = {countCentri, countVaccinati};
		return statistiche;
	}
}
	
	
